/*
 * Copyright (c) 2014 Øyvind Kolås <pippin@hodefoting.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <math.h>
#ifdef HAVE_MRG
#include "mrg.h"
#include "mrg-string.h"
#endif
#include <babl/babl.h>
#include "luz.h"
#include "luz-xml.h"
#include "luz-string.h"
#include "luz-list.h"
#include "coloritto.h"


/* For coloritto structs that are multiple coloritto entries, the first
 * entry of the collectino is concsidered representative, thus the white
 * ramp is sorted from 100% and down.
 */

// XXX: eeek mrg doesn't permit forcing a width on a span

const char *css =
"a { color: yellow; text-decoration: none;  }\n"
"div.colorinfo { font-size: 1.0em; color: white; }\n"
"div.colorspaceinfo {  }\n"
"div.colorname { margin-bottom: 0.5em; margin-top: 1.0em; }\n"
"div.colorspacename { font-weight:bold; float:left; clear: left; width: 5em; height: 1.0em; }\n"
"div.colortriplet {  }\n"
"div.palitem { float:left;width:6em;height:6em; }\n"

"span.palitemlabel { color: white; }\n"
"span.bright { color: black; }\n"

"div.append { background-color:transparent; }\n"
"div.shell { background-color:transparent;color:white; }\n"
"div.prompt { color:#7aa; display: inline; }\n"
"div.commandline { color:white; display: inline; }\n"
"div.palettetoolbar {height: 3em;color:white}\n"
"div.palettetool {border: 2px solid green; }\n"
"";

static const char *todo =
"bake queuing of draw into post argv command phase\n"
"edit colors components\n"
"show selected color in relation to whatever is loaded in 3d view\n"
"edit spectra\n"
"measure spectra\n"
"export cxf\n"
"undo/redo\n"
"storing/visualising ramps as sub-items\n"
"commandline:\n"
" fading history\n"
" splitting newlines properly\n"
"viewport:\n"
" load an image\n"
" separate mode\n"
" separate+proof\n"
" additive models in luz\n"
;

const Babl *display_rgbf = NULL;
static int palette_no = 0;

#ifdef HAVE_MRG

static void mrg_set_source_srgba (cairo_t *cr, float r, float g, float b, float a)
{
  float in_pix[4]={r,g,b,a};
  float conv_pix[4];
  babl_process (babl_fish (babl_format ("R'G'B' float"), display_rgbf),
                in_pix, conv_pix, 1);
  if (a == 1.0f)
    cairo_set_source_rgb (cr, conv_pix[0], conv_pix[1], conv_pix[2]);
  else
    cairo_set_source_rgba (cr, conv_pix[0], conv_pix[1], conv_pix[2], a);
}

static void mrg_set_source_srgb (cairo_t *cr, float r, float g, float b)
{
  mrg_set_source_srgba (cr, r, g, b, 1.0f);
}
#endif

#define MAX_KV 10

struct
_Coloritto {
  int   ref_count;
  int   kvs;
  char *keys[MAX_KV];
  char *values[MAX_KV];

  ColorittoDef set;
  ColorittoDef computed;

  /* first member is full, and first member on white is the spectrum
     propagated to 3d colors */
  Spectrum on_white;
  Spectrum on_black;
  Spectrum on_white_ramp[10];
  Spectrum on_black_ramp[10];
  /* have an enum of which ones are explicitly set */

  float scalar; /* last fallback - use for using colors as all values
                   internally */
  float lab[3];
  float display_rgb[3];
  float xyz[3];
  float xyY[3];
  float lch[3];
  float srgb[3];
  float cmyk[4];

  // CMYK


//  beware that copy by value works as long as the original
//  color is not freed - this caveat only applies to the key/value pairs
//  of the copies, not the rest of the struct.
   float delta_E;
};

struct
_ColorittoCollection {
  int   foo;
  int   kvs;
  char *keys[MAX_KV];
  char *values[MAX_KV];
  LuzList *list;
};

int use_ui = 0;
int test_precision = 0;
int use_repl = 0;



#ifdef HAVE_MRG
#define printf(foo...) \
   do{ MrgString *str = mrg_string_new_printf (foo);\
       if (use_ui) {\
       MrgString *line = mrg_string_new ("");\
       for (char *p= str->str; *p; p++) { \
         if (*p == '\n') { \
           mrg_list_prepend (&scrollback, strdup (line->str));\
           mrg_string_set (line, "");\
         } else { \
           mrg_string_append_byte (line, *p);\
         } \
       } \
       if (line->str[0]) \
         mrg_list_prepend (&scrollback, strdup (line->str));\
       mrg_string_free (line, 1);\
       }\
       else \
       { fprintf (stdout, "%s", str->str);\
       }\
       mrg_string_free (str, 1);\
   }while(0)

#endif

#ifndef MAX
#define MAX(a,b)  ((a)<(b)?(b):(a))
#endif

static Luz *luz = NULL;

void
coloritto_set (Coloritto *color, ColorittoDef color_type,
                     const float *data)
{
  switch (color_type)
  {
    case COLORITTO_SET: break;
    case COLORITTO_SCALAR:
      color->scalar = data[0];
      color->set |= color_type;
      break;
    case COLORITTO_XYZ:
      color->xyz[0] = data[0];
      color->xyz[1] = data[1];
      color->xyz[2] = data[2];
      color->set |= color_type;
      break;
    case COLORITTO_xyY:
      color->xyY[0] = data[0];
      color->xyY[1] = data[1];
      color->xyY[2] = data[2];
      color->set |= color_type;
      break;
    case COLORITTO_LAB:
      color->lab[0] = data[0];
      color->lab[1] = data[1];
      color->lab[2] = data[2];
      color->set |= color_type;
      break;
    case COLORITTO_LCH:
      color->lch[0] = data[0];
      color->lch[1] = data[1];
      color->lch[2] = data[2];
      color->set |= color_type;
      break;
    case COLORITTO_DISPLAY_RGB:
      color->display_rgb[0] = data[0];
      color->display_rgb[1] = data[1];
      color->display_rgb[2] = data[2];
      color->set |= color_type;
      break;
    case COLORITTO_SRGB:
      color->srgb[0] = data[0];
      color->srgb[1] = data[1];
      color->srgb[2] = data[2];
      color->set |= color_type;
      break;
    case COLORITTO_SPECTRUM_ON_WHITE:
      color->on_white = *((Spectrum*)data);
      color->set |= color_type;
      break;
    case COLORITTO_SPECTRUM_ON_WHITE_AND_BLACK:
      color->on_black = *((Spectrum*)data);
      color->set |= color_type;
      break;
    case COLORITTO_SPECTRUM_ON_WHITE_RAMP:
    case COLORITTO_SPECTRUM_ON_WHITE_AND_BLACK_RAMP:
      /* NYI */
      break;
  }

  color->computed = 0; // drop all caches on all sets
}

int coloritto_is_set (Coloritto *color,
                      ColorittoDef color_type)
{
  if (color->set & color_type)
    return 1;
  return 0;
}

void coloritto_ensure (Coloritto *color, ColorittoDef color_type)
{
  int got_xyz = 0;
  /* ensure that the color definition exists, */
  if (color->set & color_type)
    return;
  if (color->computed & color_type)
    return;

  if ((color->set & COLORITTO_XYZ) ||
      (color->computed & COLORITTO_XYZ))
  {
    got_xyz = 1;
  }

  if ((!got_xyz && (color->set & COLORITTO_SPECTRUM_ON_WHITE) )||
      (!got_xyz && (color->set & COLORITTO_SPECTRUM_ON_WHITE_AND_BLACK)))
  {
    luz_reflectance_to_xyz (luz, &color->on_white, &color->xyz[0],
                                                   &color->xyz[1],
                                                   &color->xyz[2]);
    color->computed |= COLORITTO_XYZ;
    got_xyz = 1;
  }
  if (!got_xyz && (color->set & COLORITTO_LAB))
  {
    babl_process (babl_fish (babl_format ("CIE Lab float"),
                  babl_format ("CIE XYZ float")),
                  color->lab, color->xyz, 1);
    color->computed |= COLORITTO_XYZ;
    got_xyz = 1;
  }
  if (!got_xyz && (color->set & COLORITTO_LCH))
  {
    babl_process (babl_fish (babl_format ("CIE LCH(ab) float"),
                  babl_format ("CIE XYZ float")),
                  color->lch, color->xyz, 1);
    color->computed |= COLORITTO_XYZ;
    got_xyz = 1;
  }
  if (!got_xyz && (color->set & COLORITTO_SRGB))
  {
    babl_process (babl_fish (babl_format ("R'G'B' float"),
                  babl_format ("CIE XYZ float")),
                  color->srgb, color->xyz, 1);
    color->computed |= COLORITTO_XYZ;
    got_xyz = 1;
  }
  if (!got_xyz && (color->set & COLORITTO_DISPLAY_RGB))
  {
    babl_process (babl_fish (display_rgbf,
                  babl_format ("CIE XYZ float")),
                  color->display_rgb, color->xyz, 1);
    color->computed |= COLORITTO_XYZ;
    got_xyz = 1;
  }
  if (!got_xyz && (color->set & COLORITTO_SCALAR))
  {
    babl_process (babl_fish (babl_format ("CIE L float"),
                  babl_format ("CIE XYZ float")),
                  &color->scalar, color->xyz, 1);
    color->computed |= COLORITTO_XYZ;
    got_xyz = 1;
  }
  if (!got_xyz && (color->set & COLORITTO_xyY))
  {
    babl_process (babl_fish (babl_format ("CIE xyY float"),
                  babl_format ("CIE XYZ float")),
                  color->xyY, color->xyz, 1);
    color->computed |= COLORITTO_XYZ;
    got_xyz = 1;
  }

 switch (color_type)
  {
    case COLORITTO_XYZ:
    case COLORITTO_SET:
      return;
    case COLORITTO_xyY:
      babl_process (babl_fish (babl_format ("CIE XYZ float"),
                    babl_format ("CIE xyY float")),
                    color->xyz, color->xyY, 1);

      color->computed |= color_type;
      break;
    case COLORITTO_SCALAR:
      babl_process (babl_fish (babl_format ("CIE XYZ float"),
                    babl_format ("CIE L float")),
                    color->xyz, color->lab, 1);
      color->computed |= color_type;
      break;
    case COLORITTO_LAB:
      babl_process (babl_fish (babl_format ("CIE XYZ float"),
                    babl_format ("CIE Lab float")),
                    color->xyz, color->lab, 1);
      color->computed |= color_type;
      break;
    case COLORITTO_LCH:
      babl_process (babl_fish (babl_format ("CIE XYZ float"),
                    babl_format ("CIE LCH(ab) float")),
                    color->xyz, color->lch, 1);
      color->computed |= color_type;
      break;
    case COLORITTO_DISPLAY_RGB:
      babl_process (babl_fish (babl_format ("CIE XYZ float"),
                    display_rgbf),
                    color->xyz, color->display_rgb, 1);
      color->computed |= color_type;
      break;
    case COLORITTO_SRGB:
      babl_process (babl_fish (babl_format ("CIE XYZ float"),
                    babl_format ("R'G'B' float")),
                    color->xyz, color->srgb, 1);
      color->computed |= color_type;
      break;

    case COLORITTO_SPECTRUM_ON_WHITE:
      coloritto_ensure (color, COLORITTO_SRGB);
      color->on_white = luz_rgb_to_spectrum (luz, color->srgb[0], color->srgb[1], color->srgb[2]);
       /* should interpolate among known spectrums, the freie farbe set
          is probably a good one, that way we generate plausible spectrums
          for all values */
      color->computed |= color_type;
      break;
    case COLORITTO_SPECTRUM_ON_WHITE_RAMP:
    case COLORITTO_SPECTRUM_ON_WHITE_AND_BLACK:
    case COLORITTO_SPECTRUM_ON_WHITE_AND_BLACK_RAMP:
      break;
  }
}

void
coloritto_get (Coloritto *color, ColorittoDef color_type,
                     float *data)
{
  coloritto_ensure (color, color_type);
  switch (color_type)
  {
    case COLORITTO_SET: break;
    case COLORITTO_SCALAR:
      data[0] = color->scalar;
      break;
    case COLORITTO_XYZ:
      data[0] = color->xyz[0];
      data[1] = color->xyz[1];
      data[2] = color->xyz[2];
      break;
    case COLORITTO_xyY:
      data[0] = color->xyY[0];
      data[1] = color->xyY[1];
      data[2] = color->xyY[2];
      break;
    case COLORITTO_LAB:
      data[0] = color->lab[0];
      data[1] = color->lab[1];
      data[2] = color->lab[2];
      break;
    case COLORITTO_LCH:
      data[0] = color->lch[0];
      data[1] = color->lch[1];
      data[2] = color->lch[2];
      break;
    case COLORITTO_DISPLAY_RGB:
      data[0] = color->display_rgb[0];
      data[1] = color->display_rgb[1];
      data[2] = color->display_rgb[2];
      break;
    case COLORITTO_SRGB:
      data[0] = color->srgb[0];
      data[1] = color->srgb[1];
      data[2] = color->srgb[2];
      break;

    case COLORITTO_SPECTRUM_ON_WHITE:
      /* just read it out */
      for (int i = 0; i < LUZ_SPECTRUM_BANDS; i ++)
        data[i] = color->on_white.bands[i];
      break;
    case COLORITTO_SPECTRUM_ON_WHITE_RAMP:
    case COLORITTO_SPECTRUM_ON_WHITE_AND_BLACK:
    case COLORITTO_SPECTRUM_ON_WHITE_AND_BLACK_RAMP:
      break;
  }
}

typedef struct
ColorLemma {
  float sx;
  float sy;
  float d;
  float radius;

  Coloritto *color;

  /* keeps many/most key=vals from parsing, but spectrum is normalized
     to internal representation - which should be at 5nm or 10nm and as wide
     as or wider than perceptual to yield accurate tristimulus values out.
    */

} ColorLemma;

LuzList *items = NULL;  /* list of color lemmas existing at all   */

Coloritto      *current = NULL;
int pal_no = 0;
int pal_replace = 0;

static int projection_model = COLORITTO_xyY;

#ifdef HAVE_MRG
MrgList *scrollback = NULL; /* scrollback buffer of free() able strings
                               with latest prepended */
#endif

char    *commandline = NULL; /* commandline being edited */

#define SCALEX     200.0
#define SCALEY     200.0
#define SRGB_SCALE 1.5

#include "luz-config.inc"

static void init_luz (void)
{
  if (!luz) luz = luz_new (config_internal);
}

Coloritto *coloritto_new (void)
{
  init_luz ();
  return calloc (sizeof (Coloritto), 1);
}

Coloritto *coloritto_ref (Coloritto *color)
{
  color->ref_count++;
  return color;
}

void coloritto_unref (Coloritto *color)
{
  color->ref_count--;
  if (color->ref_count < 0)
    free (color);
}

void coloritto_destroy (Coloritto *coloritto)
{
  coloritto_unref (coloritto);
}

void coloritto_set_key (Coloritto *color, const char *key,
                        const char *value)
{
  int i;
  for (i = 0; i < color->kvs; i++)
    if (color->keys[i] && !strcmp (key, color->keys[i]))
    {
      free (color->values[i]);
      color->values[i] = strdup (value);
      return;
    }
  if (color->kvs >= MAX_KV)
  {
    fprintf (stderr, "key-value count overflow\n");
    return;
  }

  color->keys[color->kvs] = strdup (key);
  color->values[color->kvs] = strdup (value);
  color->kvs++;
  color->keys[color->kvs] = NULL;
}

const char *coloritto_get_key (Coloritto *color, const char *key)
{
  for (int i = 0; i < color->kvs; i++)
    if (color->keys[i] && !strcmp (key, color->keys[i]))
      return color->values[i];
  return NULL;
}

void coloritto_set_name (Coloritto *color, const char *name)
{
  coloritto_set_key (color, "name", name);
}


const char *coloritto_get_name (Coloritto *color)
{
  return coloritto_get_key (color, "name");
}

#include "argvs.h"
#ifdef HAVE_MRG
Mrg *mrg = NULL;
#endif

static Coloritto * add_item (
      ColorittoDef color_type,
      float *triplet,
      float radius,
      ColorittoCollection *cc)
{
  ColorLemma *item = calloc (sizeof (ColorLemma), 1);
  item->color = coloritto_new ();
  init_luz (); // we do it here and in the actual public allocation for colorittos
  coloritto_set (item->color, color_type, triplet);
  item->radius = radius;
  luz_list_prepend (&items, item);
  if (cc)
    coloritto_collection_set_no (cc, -1, item->color);
  return item->color;
}

int cmd_todo (COMMAND_ARGS) /* "todo", 0, "", "print coloritto todolist" */
{
  printf ("%s", todo);
  return 0;
}

int cmd_locus (COMMAND_ARGS) /* "locus", 0, "", "adds the spectral locus as 3d color elements" */
{
  float xyz[3];
  const Spectrum *xs = luz_get_spectrum (luz, "observer_x");
  const Spectrum *ys = luz_get_spectrum (luz, "observer_y");
  const Spectrum *zs = luz_get_spectrum (luz, "observer_z");

  for (float nm = 400; nm < 690; nm += 1)
  {
    char name[32];
    xyz[0] = luz_spectrum_interpolate_nm (xs, nm);
    xyz[1] = luz_spectrum_interpolate_nm (ys, nm);
    xyz[2] = luz_spectrum_interpolate_nm (zs, nm);
    Coloritto *color = add_item (COLORITTO_XYZ, &xyz[0], 0.005, 0);
    sprintf (name, "%.0fnm", nm);
    coloritto_set_name (color, name);
  }
#ifdef HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

int cmd_rgb_primaries (COMMAND_ARGS) /* "rgb_primaries", 0, "", "adds the rgb primaries" */
{
  {
    float rgb[3]={1,0,0};
    add_item (COLORITTO_DISPLAY_RGB, &rgb[0], 0.01, 0);
  }
  {
    float rgb[3]={0,1,0};
    add_item (COLORITTO_DISPLAY_RGB, &rgb[0], 0.01, 0);
  }
  {
    float rgb[3]={0,0,1};
    add_item (COLORITTO_DISPLAY_RGB, &rgb[0], 0.01, 0);
  }
  return 0;
}


int cmd_rgb_primaries2 (COMMAND_ARGS) /* "srgb_primaries", 0, "", "adds the srgb primaries" */
{
  {
    float rgb[3]={1,0,0};
    add_item (COLORITTO_SRGB, &rgb[0], 0.01, 0);
  }
  {
    float rgb[3]={0,1,0};
    add_item (COLORITTO_SRGB, &rgb[0], 0.01, 0);
  }
  {
    float rgb[3]={0,0,1};
    add_item (COLORITTO_SRGB, &rgb[0], 0.01, 0);
  }
  return 0;
}

static void add_items ()
{
  //float lab[3];

#if 0
  float rgb[3];
  for (rgb[0] = 0.0; rgb[0] <= 1.0; rgb[0] += 0.1)
  for (rgb[1] = 0.0; rgb[1] <= 1.0; rgb[1] += 0.1)
  for (rgb[2] = 0.0; rgb[2] <= 1.0; rgb[2] += 0.1)
  {
    add_item (COLORITTO_RGB, &rgb[0], 0.01);
  }
#endif

#if 0

  lab[1] = 0.0;
  lab[2] = 0.0;
  for (lab[0] = 50.0; lab[0] <= 100.0; lab[0] += 30.0)
  {
    add_item (COLORITTO_LAB, &lab[0], 0.01);
  }


  lab[1] = 0.0;
  lab[2] = 0.0;
  for (lab[0] = 0.0; lab[0] <= 50.0; lab[0] += 30.0)
  {
    add_item (COLORITTO_LAB, &lab[0], 0.01);
  }

 #if 0
  lab[0] = 100.0;
  for (lab[1] = -200.0; lab[1] <= 200.0; lab[1] += 30.0)
  for (lab[2] = -200.0; lab[2] <= 200.0; lab[2] += 30.0)
  {
    add_item (COLORITTO_LAB, &lab[0], 0.0025);
  }
#endif

#endif
}

static void *list_nth_data (LuzList *list, int nth)
{
  LuzList *l = luz_list_nth(list, nth);
  if (l)
   return l->data;
  return NULL;
}


int cmd_ax (COMMAND_ARGS) /* "rainbow", 0, "", "draw cie lch rainbow" */
{
  float lch[3] = {50, 95.0, 0.0};
  for (lch[2] = 0.0; lch[2] <= 360.0; lch[2] += .5)
    add_item (COLORITTO_LCH, &lch[0], 0.0025, 0);

  return 0;
}


int cmd_lab (COMMAND_ARGS) /* "lab", 0, "", "switch to CIE Lab space in 3d view" */
{
  projection_model = COLORITTO_LAB;
#ifdef HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

int cmd_xyY (COMMAND_ARGS) /* "xyY", 0, "", "switch to xyY space in 3d view" */
{
  projection_model = COLORITTO_xyY;
#ifdef HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

int cmd_xyz (COMMAND_ARGS) /* "xyz", 0, "", "switch to CIE XYZ color space in 3d view" */
{
  projection_model = COLORITTO_XYZ;
#ifdef HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

int cmd_rgb (COMMAND_ARGS) /* "rgb", 0, "", "switch to sRGB view in 3d space" */
{
  projection_model = COLORITTO_SRGB;
#ifdef HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

#ifdef HAVE_MRG

#define ARGV_BOOL(name) \
  if (argv[1]) \
  {            \
    name = atoi(argv[1]); \
    if (!strcmp (argv[1], "on")) name = 1; \
  } \
  else \
   name = !name; \
  printf (#name " is %s", name?"on":"off"); \
  mrg_queue_draw (mrg, NULL); \
  return 0

#else

#define ARGV_BOOL(name) \
  if (argv[1]) \
  {            \
    name = atoi(argv[1]); \
    if (!strcmp (argv[1], "on")) name = 1; \
  } \
  else \
   name = !name; \
  printf (#name " is %s", name?"on":"off"); \
  return 0

#endif


static float rotation_angle = 0.0;
static int ortho = 1;
static int spin = 0;
static int axes = 1;
static int crosshair = 1;

int cmd_crosshair (COMMAND_ARGS) /* "crosshair", 0, "[on|off]", "toggle crosshair or set expiclit mode" */
{

  ARGV_BOOL(crosshair);
}

int cmd_ortho (COMMAND_ARGS) /* "ortho", 0, "[on|off]", "toggle orthographic or perspective rendering" */
{
  ARGV_BOOL(ortho);
}

int cmd_axes (COMMAND_ARGS) /* "axes", 0, "[on|off]", "toggle drawing of axis lines in 3d view" */
{
  ARGV_BOOL(axes);
}

int cmd_spin (COMMAND_ARGS) /* "spin", 0, "[on|off]", "toggle automatic spinning of 3d view" */
{
  ARGV_BOOL(spin);
}

LuzList *palettes = NULL;


static void update_current (void)
{
  ColorittoCollection *cc = list_nth_data (palettes, palette_no);
  LuzList *palette = cc->list;
  int no = 0;
  for (LuzList *l = palette; l; l = l->next, no ++)
  {
    if (no == pal_no)
    {
      current = l->data;
      return;
    }
  }
}


int cmd_pno (COMMAND_ARGS) /* "pno", 0, "[no]", "sets the current palette no" */
{
  if (argv[1])
  {
    palette_no = atoi(argv[1]);
    if (palette_no < 0)
     palette_no = 0;
    if (palette_no > luz_list_length (palettes) - 1)
     palette_no = luz_list_length (palettes) - 1;
  }
  else
    palette_no = 0;
#ifdef HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

int cmd_rotate (COMMAND_ARGS) /* "rotate", 0, "[degrees]", "sets the degrees of rotation around y axis in 3d view, with no arguments reset to 0" */
{
  if (argv[1])
  {
    rotation_angle = strtod(argv[1], NULL);
  }
  else
    rotation_angle = 0.0;
  spin = 0;
#ifdef HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

static float cos_rotation;
static float sin_rotation;

void xyz_to_xy (float x, float y, float z,
                float *sx, float *sy, float *d)
{
   {
     float u,v,w;

     u = x * cos_rotation - z * sin_rotation;
     v = y;
     w = x * sin_rotation + z * cos_rotation;

     x=u; y = v; z = w;

   }

   if (ortho)
   {
     if (d)   *d = z;
     if (sx) *sx = x + 0.5;
     if (sy) *sy = y + 0.5;
   }
   else
   {
     float eyed = 1.0;
     float dist = 1.0;

/*
                     ,`
              /    ,` |
             /|  ,`   |
            / |,`     | y
           / ,`sy     |
      eye /,`_|_______|
          \eyed+dist+z
           \  |
            \ |
             \|
              \

    sy / eyed  =  y / ( eyed + dist + z );

    sy =  y * eyed / ( eyed + dist + z ) + 0.5;
*/

     if (d) *d = dist + z;
     if (sx) *sx =  x * eyed / ( eyed + dist + z ) + 0.5;
     if (sy) *sy =  y * eyed / ( eyed + dist + z ) + 0.5;

   }
}

static int compare_delta_e (const void *a, const void *b, void *userdata)
{
  const Coloritto *la = a;
  const Coloritto *lb = b;

  if (la->delta_E < lb->delta_E)
    return -1;
  else if (la->delta_E > lb->delta_E)
    return 1;
  return 0;
}

#ifdef HAVE_MRG
static int compare_depths (const void *a, const void *b, void *userdata)
{
  const ColorLemma *la = a;
  const ColorLemma *lb = b;
  return (la->d - lb->d) * 100;
}

static void update_projection ()
{
  /* here 3d projection and rotation can be incorporated */
  if (spin)
  {
    rotation_angle += 0.02;
    mrg_queue_draw (mrg, NULL);
  }
  cos_rotation = cos (rotation_angle);
  sin_rotation = sin (rotation_angle);

  for (LuzList *l = items; l; l = l->next)
  {
    ColorLemma *item = l->data;
    coloritto_ensure (item->color, COLORITTO_SRGB);
    coloritto_ensure (item->color, COLORITTO_DISPLAY_RGB);
    float x, y, z;
    x = y = z = 0.0f;

    coloritto_ensure (item->color, projection_model);

    switch (projection_model)
    {
      case COLORITTO_xyY:
        x = item->color->xyY[0] - 0.5;
        y = 1.0-item->color->xyY[1] - 0.5;
        z = item->color->xyY[2] - 0.5;
        break;
      case COLORITTO_LAB:
        x = (item->color->lab[1])/SCALEX;
        y = item->color->lab[2]/SCALEX;
        z = (item->color->lab[0]-50)/SCALEY;
        break;
      case COLORITTO_SRGB:
        x = (item->color->srgb[0]-0.5) / SRGB_SCALE ;
        y = -(item->color->srgb[1]-0.5) / SRGB_SCALE ;
        z = (item->color->srgb[2]-0.5) / SRGB_SCALE ;

        break;

      case COLORITTO_XYZ:
        x = (item->color->xyz[0] - 0.5) / SRGB_SCALE;
        y = -(item->color->xyz[1] - 0.5) / SRGB_SCALE;
        z = (item->color->xyz[2] - 0.5) / SRGB_SCALE;
        break;
    }

    xyz_to_xy (x, y, z, &item->sx, &item->sy, &item->d);
  }
  luz_list_sort (&items, compare_depths, NULL);
}

static void do_cmd (MrgEvent *event, void *data1, void *data2) {
  argvs_eval (data1);
  mrg_event_stop_propagate (event);
  mrg_queue_draw (event->mrg, NULL);
}

static void select_item_cb (MrgEvent *event, void *data1, void *data2) {
  ColorittoCollection *cc = list_nth_data (palettes, palette_no);
  LuzList *palette = cc->list;

  current = data1;
  if (pal_replace)
  {
    int no  = 0;

    for (LuzList *l = palette; l; l = l->next, no ++)
    {
      if (no == pal_no)
      {
        l->data = current;
        break;
      }
    }
  }
  mrg_event_stop_propagate (event);
}

static void clicked_palette (MrgEvent *event, void *data1, void *data2) {
  ColorittoCollection *cc = list_nth_data (palettes, palette_no);
  LuzList *palette = cc->list;
  if (pal_no == (size_t) data2)
    {
      pal_replace = !pal_replace;
      mrg_event_stop_propagate (event);
      mrg_queue_draw (event->mrg, NULL);
      return;
    }
  pal_no = (size_t) data2;   /* set the index# to activate */
  pal_replace = 0;


  if (luz_list_length (palette) <= pal_no)
    {
      if (!items)
        return;
#if 0
      /* should invent a new blank color starting from black instead */
      ColorLemma *lemma = items->data;
      Coloritto  *color = &lemma->color;
#else
      Coloritto  *color = coloritto_new ();
#endif
      luz_list_append (&palette, color);
    }

  update_current ();
  mrg_event_stop_propagate (event);
  mrg_queue_draw (event->mrg, NULL);
}

static void evaluate_commandline (MrgEvent *event, void *data1, void *data2) {
  argvs_eval (commandline);
  free (commandline);
  commandline = strdup ("");
  mrg_set_cursor_pos (mrg, 0);
  mrg_event_stop_propagate (event);
  mrg_queue_draw (event->mrg, NULL);
}

static void drag_void (MrgEvent *event, void *data1, void *data2)
{
  rotation_angle += event->delta_x / mrg_height (event->mrg) * 3.1415 * 2;
}

void select_none (void)
{
}

void make_collection_selection (void)
{
}

void save_selection (void)
{

}

//#define MAYBE_CENTER(x)  ( (x) * h + (w - h) / 2)
#define MAYBE_CENTER(x)  ((x) * h)


static void ui_axes (Mrg *mrg, void *data)
{
  cairo_t *cr = mrg_cr (mrg);
  float h = mrg_height (mrg);
  const Spectrum *xs = luz_get_spectrum (luz, "observer_x");
  const Spectrum *ys = luz_get_spectrum (luz, "observer_y");
  const Spectrum *zs = luz_get_spectrum (luz, "observer_z");

  switch (projection_model)
  {
    case COLORITTO_xyY:
      {
        float sx, sy;
#define C(x,y,z) \
        xyz_to_xy ((x-0.5),1.0-y-0.5,(z-0.5), &sx, &sy, NULL); sx = MAYBE_CENTER(sx); sy *= h;

        cairo_new_path (cr);
        for (float nm = 400; nm < 690; nm += 2)
        {
           float xyz[3];
           xyz[0] = luz_spectrum_interpolate_nm (xs, nm);
           xyz[1] = luz_spectrum_interpolate_nm (ys, nm);
           xyz[2] = luz_spectrum_interpolate_nm (zs, nm);
           float sum = xyz[0] + xyz[1] + xyz[2];

           C(xyz[0]/sum,xyz[1]/sum,xyz[1]); cairo_line_to (cr, sx, sy);
        }
        cairo_close_path (cr);

        cairo_set_source_rgb (cr, 1,1,1);
        cairo_set_line_width (cr, 1);
        cairo_stroke (cr);
      }
      break;
#undef C

    case COLORITTO_XYZ:
      {
        float sx, sy;
#define C(x,y,z) \
        xyz_to_xy ((x-0.5)/SRGB_SCALE,-(y-0.5)/SRGB_SCALE,(z-0.5)/SRGB_SCALE, &sx, &sy, NULL); sx = MAYBE_CENTER(sx); sy *= h;

        C(0,0,0); cairo_move_to (cr, sx, sy);
        C(1,0,0); cairo_line_to (cr, sx, sy);
        C(0,0,0); cairo_move_to (cr, sx, sy);
        C(0,1,0); cairo_line_to (cr, sx, sy);
        C(0,0,0); cairo_move_to (cr, sx, sy);
        C(0,0,1); cairo_line_to (cr, sx, sy);
        C(0,0,0); cairo_move_to (cr, sx, sy);
        C(1,1,1); cairo_line_to (cr, sx, sy);

        cairo_set_source_rgb (cr, 1,1,1);
        cairo_set_line_width (cr, 1);
        cairo_stroke (cr);
      }
      break;
    case COLORITTO_SRGB:
     {
        float sx, sy;
        cairo_set_line_width (cr, 1);

        C(0,0,0); cairo_move_to (cr, sx, sy);
        C(1,0,0); cairo_line_to (cr, sx, sy);
        cairo_set_source_rgb (cr, 1,0,0);
        cairo_stroke (cr);

        C(0,0,0); cairo_move_to (cr, sx, sy);
        C(0,1,0); cairo_line_to (cr, sx, sy);
        cairo_set_source_rgb (cr, 0,1,0);
        cairo_stroke (cr);

        C(0,0,0); cairo_move_to (cr, sx, sy);
        C(0,0,1); cairo_line_to (cr, sx, sy);
        cairo_set_source_rgb (cr, 0,0,1);
        cairo_stroke (cr);


        C(0,0,0); cairo_move_to (cr, sx, sy);
        C(1,1,1); cairo_line_to (cr, sx, sy);
        cairo_set_source_rgb (cr, 1,1,1);
        cairo_stroke (cr);

      }
      break;
#undef C
    case COLORITTO_LAB:
      {
        float sx, sy;
#define C(x,y,z) \
        xyz_to_xy ((y)/SCALEX,z/SCALEX,(x-50)/SCALEY, &sx, &sy, NULL); sx = MAYBE_CENTER(sx); sy *= h;

        cairo_set_line_width (cr, 1);
        C(0,0,0); cairo_move_to (cr, sx, sy);
        C(100,0,0); cairo_line_to (cr, sx, sy);

        cairo_set_source_rgb (cr, 1,1,1);
        cairo_stroke (cr);
      }

      break;
  }
}

// performance hack, cache sx and sy of current
static float current_sx;
static float current_sy;

static void ui_items (Mrg *mrg, void *data)
{
  float w = mrg_width (mrg);
  float h = mrg_height (mrg);
  cairo_t *cr = mrg_cr (mrg);
  cairo_set_antialias (cr, CAIRO_ANTIALIAS_FAST);
  int length = luz_list_length (items);

  cairo_new_path (cr);
  cairo_rectangle (cr, 0, 0, w, h);
  mrg_listen (mrg, MRG_DRAG, drag_void, NULL, NULL);
  cairo_new_path (cr);
  //fprintf (stdout, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

  for (LuzList *l = items; l; l = l->next)
  {
    ColorLemma *lemma = l->data;
    Coloritto  *color = lemma->color;
    float rad_scale = 1.0;

    if (lemma->d < 0.0 && !ortho) continue;

    if (current == color){
       current_sx = lemma->sx;
       current_sy = lemma->sy;
    }

    cairo_arc (cr, MAYBE_CENTER (lemma->sx), lemma->sy * h,
      lemma->radius * h * rad_scale, 0.0, M_PI * 2);

    if (length > 100)
      cairo_set_source_rgba (cr, color->display_rgb[0], color->display_rgb[1], color->display_rgb[2], 0.65);
    else
      cairo_set_source_rgb (cr, color->display_rgb[0], color->display_rgb[1], color->display_rgb[2]);

    mrg_listen (mrg, MRG_PRESS, select_item_cb, color, NULL);

    cairo_fill (cr);


    if(0){
      const char *name = coloritto_get_name (color);
      char *tmp = strdup (name);
      int H = atoi (tmp+1);
      int L = atoi (tmp+6);
      int C = atoi (tmp+10);

      float triplet[3];
      coloritto_get (color, COLORITTO_LCH, triplet);
      if (fabs (H-triplet[2] + fabs (L-triplet[0] + fabs (C - triplet[1])) > 7.5))
      {

      if(1)fprintf (stdout, "\n%s\nH%.2f L%.2f C%.2f - %i %i %i\n\n",
          coloritto_get_name (color),
          triplet[2], triplet[0], triplet[1], H, L, C);
      if(0)fprintf (stdout, "\n%s\nH%03.0f_L%02.0f_C%03.0f\n\n",
          coloritto_get_name (color),
          round(triplet[2]), round(triplet[0]), round(triplet[1]));
      }
    }
  }
}

void ui_color_info (Mrg *mrg, void *data, Coloritto *color)
{
  MrgString *str = mrg_string_new ("");
  float em = mrg_em (mrg);
  mrg_start (mrg, "div.colorinfo", NULL);
  //mrg_set_edge_left (mrg, mrg_width (mrg) - 6.4 * em * 6);
  mrg_set_edge_left (mrg, 0);
  mrg_set_xy (mrg, 0, mrg_height (mrg) * 0.5);

  mrg_string_append_printf (str, "<div class='colorname'>%s</div>", coloritto_get_name (current));
    {
      float triplet[3];
      coloritto_get (current, COLORITTO_LAB, triplet);
      mrg_string_append_printf (str, "<div class='colorspaceinfo'><div class='colorspacename'>CIE Lab</div> <div class='colortriplet'>%.5f %.5f %.5f</div> %s</div>\n",
          triplet[0], triplet[1], triplet[2], coloritto_is_set (current, COLORITTO_LAB)?"*":"");
    }

    {
      float triplet[3];
      coloritto_get (current, COLORITTO_LCH, triplet);
      mrg_string_append_printf (str, "<div class='colorspaceinfo'><div class='colorspacename'>CIE LCH</div> <div class='colortriplet'>%.5f %.5f %.5f</div> %s</div>\n",
          triplet[0], triplet[1], triplet[2], coloritto_is_set (current, COLORITTO_LCH)?"*":"");
    }

    {
      float triplet[3];
      coloritto_get (current, COLORITTO_XYZ, triplet);
      mrg_string_append_printf (str, "<div class='colorspaceinfo'><div class='colorspacename'>CIE XYZ</div><div class='colortriplet'>%.5f %.5f %.5f</div> %s</div>\n",
          triplet[0], triplet[1], triplet[2], coloritto_is_set (current, COLORITTO_XYZ)?"*":"");
    }

    {
      float triplet[3];
      coloritto_get (current, COLORITTO_xyY, triplet);
      mrg_string_append_printf (str, "<div class='colorspaceinfo'><div class='colorspacename'>CIE xyY</div><div class='colortriplet'>%.5f %.5f %.5f</div> %s</div>\n",
          triplet[0], triplet[1], triplet[2], coloritto_is_set (current, COLORITTO_xyY)?"*":"");
    }

    {
      float triplet[3];
      coloritto_get (current, COLORITTO_SRGB, triplet);
      mrg_string_append_printf (str, "<div class='colorspaceinfo'><div class='colorspacename'>sRGB</div><div class='colortriplet'>%.4f %.4f %.4f</div> %s</div>\n",
          triplet[0] * 255, triplet[1] * 255, triplet[2] * 255, coloritto_is_set (current, COLORITTO_SRGB)?"*":"");
    }

    {
      float triplet[3];
      coloritto_get (current, COLORITTO_DISPLAY_RGB, triplet);
      mrg_string_append_printf (str, "<div class='colorspaceinfo'><div class='colorspacename'>dRGB</div><div class='colortriplet'>%.4f %.4f %.4f</div> %s</div>\n",
          triplet[0] * 255, triplet[1] * 255, triplet[2] * 255, coloritto_is_set (current, COLORITTO_DISPLAY_RGB)?"*":"");
    }

  mrg_print_xml (mrg, str->str);
  mrg_string_free (str, 1);

   {
      float x0, y0, x1, y1;
      cairo_t *cr = mrg_cr (mrg);

      Spectrum spec;
      x0 = 0;
      x1 = 6 * 6.6 * em;
      y1 = mrg_height (mrg);
      y0 = mrg_height (mrg) * 0.75;

      cairo_new_path (cr);
      coloritto_get (current, COLORITTO_SPECTRUM_ON_WHITE, (float*)&spec);
      for (int i = 0; i < LUZ_SPECTRUM_BANDS; i++)
      {
        cairo_line_to (cr, x0 + (i * 1.0f / LUZ_SPECTRUM_BANDS) * (x1-x0),
                           y1 - spec.bands[i] * (y1-y0));
      }
      if (coloritto_is_set (current, COLORITTO_SPECTRUM_ON_WHITE))
        cairo_set_source_rgb (cr, 1, 1, 1);
      else
        cairo_set_source_rgba (cr, 1, 1, 1, 0.5);
      cairo_stroke (cr);

   }

  mrg_end (mrg);
}

static void link_argvs_eval (MrgEvent *event, void *href, void *link_data) {
  argvs_eval (href);
  //fprintf (stderr, "%s\n", href);
}

int ui_palette (Mrg *mrg, void *data)
{
  ColorittoCollection *cc = list_nth_data (palettes, palette_no);
  LuzList *palette = cc->list;
  cairo_t *cr = mrg_cr (mrg);
  MrgString *str = mrg_string_new ("");
  LuzList *l;
  float em = mrg_em (mrg);
  int no = 0;
  int row_max = 5;
  int row_count = 0;
  int row_no = 0;
  int current_no = 0;
  mrg_start (mrg, "div.palette", NULL);

  //mrg_set_edge_left (mrg, mrg_width (mrg) - 6.4 * em * 6);
  mrg_set_edge_left (mrg, 0);
  mrg_set_edge_right (mrg, 6.4 * em * 6);
  mrg_set_xy (mrg, 0, 0); //em * 20);
  //cairo_rectangle (cr, mrg_width (mrg) - 6.4 * em * 6, 0, mrg_width (mrg), mrg_height (mrg) * 0.5
  cairo_rectangle (cr, 0, 0, mrg_width (mrg), mrg_height (mrg) * 0.5
  );
  cairo_clip (cr);
// ortho rotate 
  mrg_start (mrg, "div.palettetoolbar", NULL);
    mrg_start (mrg, "div.palettetool", NULL);
    mrg_xml_renderf (mrg, NULL, link_argvs_eval, NULL,
                     "Coloritto <a href='pal-remove'>cut</a>|<a href='pal-copy'>copy</a>|<a href='pal-paste'>paste</a>|<a href='q'>quit</a><br/>");
    mrg_end (mrg);

  mrg_end (mrg);
  mrg_print_xml (mrg, "<span style='clear:both'/>");

  l = palette;
  {
    current_no = pal_no;
    int skip = ((current_no / row_max) - 2) * row_max;
    if (skip > 0)
    {
      int skipped = 0;
      for (; l && skipped < skip; l = l->next, skipped++, no++);
    }
  }

  row_count = 0;
  row_no = 0;

  for (; l && row_no < 10; l = l->next, no++)
  {
    Coloritto *color = l->data;
    coloritto_ensure (color, COLORITTO_DISPLAY_RGB);

    mrg_string_append_printf (str, "<div class='palitem' style='border:2px solid %s;background-color:rgb(%f %f %f)'> <span style='%s' class='palitemlabel'>%s</span> <span style='%s'>%s</span></div>",
          no == pal_no ? "white" : "gray",
          color->display_rgb[0] * 255,
          color->display_rgb[1] * 255,
          color->display_rgb[2] * 255,
          color->xyz[1] > 0.5 ? "color:#777":"color:white",
          coloritto_get_name (color),
          color->xyz[1] > 0.5 ? "color:#777":"color:white",
          no == pal_no && pal_replace?"replace":"");

    cairo_rectangle (cr, mrg_edge_left (mrg) + em * 6.9 * row_count, mrg_y (mrg), em * 6, em * 6);
    mrg_listen (mrg, MRG_PRESS, clicked_palette, color, (void*)(size_t)no);
    //cairo_set_source_rgb (cr, 1,0,0);
    //cairo_stroke (cr);
    cairo_new_path (cr);
    row_count++;

    if (row_count >= row_max)
    {
      mrg_print_xml (mrg, str->str);
      mrg_print_xml (mrg, "<span style='clear:both'/>");
      mrg_string_set (str, "");

      row_count = 0;
      row_no++;
    }
  }

  {
    mrg_string_append_printf (str, "<div class='palitem append' style='border:2px solid %s;background-color:transparent;'> <span class='palitemlabel' style='background:transparent; font-size:3em;'>+</span> </div>", "gray");
  }
  cairo_rectangle (cr, mrg_edge_left (mrg) + em * 6.9 * row_count, mrg_y (mrg), em * 6, em * 6);
  mrg_listen (mrg, MRG_PRESS, clicked_palette, NULL, (void*)(size_t)no);
  cairo_new_path (cr);

  mrg_print_xml (mrg, str->str);

  mrg_string_free (str, 1);

  mrg_end (mrg);
  return 0;
}

static void update_string (const char *new_string, void *user_data)
{
  char **string_loc = user_data;
  free (*string_loc);
  *string_loc = strdup (new_string);
}

static void ui_commandline (Mrg *mrg, void *data)
{
  float em = mrg_em (mrg);
  float h = mrg_height (mrg);
  cairo_t *cr = mrg_cr (mrg);
  int row = 1;
  cairo_save (cr);
  cairo_translate (cr, em * 6 * 6.5, 0);
  mrg_set_xy (mrg, em, h - em * 1.2 * row);
  mrg_start (mrg, "div.shell", NULL);
  mrg_set_edge_left (mrg, em);
  mrg_start (mrg, "div.prompt", NULL);
  mrg_printf (mrg, "> ");
  mrg_end (mrg);
  mrg_start (mrg, "div.commandline", NULL);
  mrg_edit_start (mrg, update_string, &commandline);
  mrg_printf (mrg, "%s", commandline);
  mrg_end (mrg);
  mrg_edit_end (mrg);
  row++;

  for (MrgList *l = scrollback; l && row * em * 1.2 < h; l = l->next, row++)
  {
    mrg_set_xy (mrg, em, h - em * 1.2 * row);
    mrg_printf (mrg, "%s", l->data);
  }
  mrg_end (mrg);

  mrg_add_binding (mrg, "return", NULL, NULL, evaluate_commandline, NULL);
  cairo_restore (cr);
}

static void
ui_viewport (Mrg *mrg, void *data)
{
  float w = mrg_width (mrg);
  float h = mrg_height (mrg);
  float em = mrg_em (mrg);

  cairo_t *cr = mrg_cr (mrg);
  cairo_save (cr);

  /*  by setting it in srgb with mrg we get color management,
   *  color management that most truely works when 
   */
  mrg_set_source_srgb (cr, .4, .4, .4);
  cairo_paint (cr); /* todo: use a form of structured noise here */
  cairo_translate (cr, em * 6 * 6.5, 0);

  mrg_start (mrg, "div.viewporttoolbar", NULL);
    mrg_start (mrg, "div.viewporttool", NULL);
    mrg_xml_renderf (mrg, NULL, link_argvs_eval, NULL,
                     "<a href='lab'>CIE lab</a>|<a href='xyY'>CIE xyY</a>|<a href='xyz'>CIE XYZ</a>|<a href='rgb'>sRGB</a><br/><a href='spin'>spin</a> | <a href='rotate'>rotation=0</a>|<a href='rotate -1.5707'>rotation=90</a> <br/> <a href='ortho'>ortho</a><br/><a href='axes'>axes</a>|<a href='crosshair'>crosshair</a>   <br/><a href='clear'>clear</a> ");
    mrg_end (mrg);
  mrg_end (mrg);

  update_projection ();
  if (axes)
    ui_axes (mrg, data);
  ui_items (mrg, data);

  if (current && crosshair)
   {
     float sx = MAYBE_CENTER (current_sx);
     float sy = current_sy * h;
     float em = mrg_em (mrg);

     cairo_move_to (cr, sx, 0);
     cairo_line_to (cr, sx, sy - em);
     cairo_move_to (cr, sx, h);
     cairo_line_to (cr, sx, sy + em);
     cairo_move_to (cr, 0, sy);
     cairo_line_to (cr, sx - em, sy);
     cairo_move_to (cr, w, sy);
     cairo_line_to (cr, sx + em, sy);

     cairo_set_source_rgb (cr, 1,1,1);
     cairo_stroke (cr);
   }
  cairo_restore (cr);
}

static void
ui (Mrg *mrg, void *data)
{
  mrg_stylesheet_add (mrg, css, NULL, 0, NULL);

  ui_viewport (mrg, data);
  ui_commandline (mrg, data);

  if (current)
    ui_color_info (mrg, data, current);

  ui_palette (mrg, data);

#define binding(keystring,command) \
  mrg_add_binding (mrg, keystring, NULL, NULL, do_cmd, command);
  binding( "tab",       "spin");
  binding( "up",        "pal-prev");
  binding( "down",      "pal-next");
  binding( "shift-up",  "pal-swap-prev");
  binding( "shift-down","pal-swap-next");
  binding( "control-x", "pal-remove");
  binding( "control-c", "pal-copy");
  binding( "control-v", "pal-paste");
  binding( "control-q", "q");
#undef binding


}

#endif

static int
file_get_contents (const char  *path,
                   char       **contents,
                   long        *length)
{
  FILE *file;
  long  size;
  long  remaining;
  char *buffer;

  file = fopen (path, "rb");

  if (!file)
    return -1;

  fseek (file, 0, SEEK_END);
  *length = size = remaining = ftell (file);
  rewind (file);
  buffer = malloc(size + 8);

  if (!buffer)
    {
      fclose(file);
      return -1;
    }

  remaining -= fread (buffer, 1, remaining, file);
  if (remaining)
    {
      fclose (file);
      free (buffer);
      return -1;
    }
  fclose (file);
  *contents = buffer;
  buffer[size] = 0;
  return 0;
}

static int has_suffix (const char *str, const char *suffix)
{
  int len = strlen (str);
  int suffix_len = strlen (suffix);

  if (suffix_len > len)
    return 0;

  if (!strcmp (str + len - suffix_len, suffix))
    return 1;
  return 0;
}

typedef enum XmlState {
  IN_NONE = 0,
  IN_XYZ_TAG,
  IN_XYZ_X_TAG,
  IN_XYZ_Y_TAG,
  IN_XYZ_Z_TAG,
  IN_CIE_TAG,
  IN_CIE_L_TAG,
  IN_CIE_A_TAG,
  IN_CIE_B_TAG,
  IN_LAB_TAG,
  IN_LAB_L_TAG,
  IN_LAB_A_TAG,
  IN_LAB_B_TAG,
  IN_SRGB_TAG,
  IN_SRGB_RED_TAG,
  IN_SRGB_GREEN_TAG,
  IN_SRGB_BLUE_TAG,
  IN_SPECTRAL_TAG,
  IN_ILLUMINANT_TAG,
  IN_DESCRIPTION_TAG,
  IN_COLOR_DEPTH_TAG,
  IN_REFLECTANCE_POINT_TAG,
  IN_REFLECTANCE_SPECTRUM_TAG,
} XmlState;


static void parse_config_line (Luz *luz, const char *line, ColorittoCollection *cc)
{
  char *key;
  char *val;
  char *p;
  char *rest;
  if (!line)
    return;

  if (!strchr (line, '=')) /* lines without = are simply skipped */
    return;
  while (*line == ' ') line++;

  key = strdup (line);
  p = rest = strchr (key, '=');

  while (*rest == '=' || *rest == ' ')
          rest++;
  while (*p == '=' || *p == ' ')
  {
    *p = '\0';
    p--;
  }
  val = rest;
/***** key and val are setup ******/

  {
    int spaces = 0;
    float vals[128]={};

    vals[spaces]=strtod(rest, NULL);

    for (int n = 0; rest[n]; n++)
    {
      if (rest[n] == ' ')
      {
        spaces++;
        //while (rest[n+1] == ' ') n++; // skip internal spaces
        vals[spaces]=strtod(&rest[n+1], NULL);
      }
    }

    if (spaces < 1)
      ; //fprintf (stderr, "too ignore... [%s] = scalar[%s]\n", key, rest);
    else if (spaces < 4)
    {
      Coloritto *color = add_item (COLORITTO_SRGB, &vals[1], 0.004, cc);
      coloritto_set_name (color, key);
    }
    else if (spaces > 6)
    {
      Spectrum spectrum = luz_parse_spectrum (luz, rest);
      Coloritto *color = add_item(COLORITTO_SPECTRUM_ON_WHITE, (void*)&spectrum, 0.008, cc);
      coloritto_set_name (color, key);
    }
  }

  free (key);
}

static void
cluz_parse_int (Luz *luz, const char *p, ColorittoCollection *cc)
{
  char acc[4096]; /* XXX: not protected */
  int acci = 0;
  acci = 0;
  while (*p)
  {
    switch (*p)
    {
      case '\n':
        parse_config_line (luz, acc, cc);
        acci = 0;
        acc[acci] = 0;
        break;
      default:
        acc[acci++] = *p;
        acc[acci] = 0;
        break;
    }
    p++;
  }
  parse_config_line (luz, acc, cc);
}

void
load_txt (ColorittoCollection *cc, Luz *luz, const char *path)
{
  char *contents = NULL;
  long length;
  file_get_contents (path, &contents, &length);
  if (contents)
  {
    cluz_parse_int (luz, contents, cc);
    free (contents);
  }
}

void
coloritto_set_display_icc (const char *icc_data, int icc_length)
{
  const Babl *display_space;
  display_space = babl_icc_make_space (icc_data, icc_length, BABL_ICC_INTENT_RELATIVE_COLORIMETRIC, NULL);

  display_rgbf = babl_format_with_space ("R'G'B' float", display_space);
}

ColorittoDef print_spaces = 0;

void
analyze_precision (ColorittoCollection *cc)
{
  int count = coloritto_collection_count (cc);

  int total = 0;
  double sum_delta_E = 0.0;
  double min_delta_E = 100.0;
  double max_delta_E = 0.0;
  int    max_no = 0;

  for (int i = 1; i <= count; i++)
  {
    Coloritto *color = coloritto_collection_get_no (cc, i);
    const char *name = coloritto_get_name (color);

    if (name[0] == 'H' &&
        name[4] == '_' &&
        name[5] == 'L')
    {
      float exact_LCH[3];
      float exact_Lab[3];
      //double delta_E;

      exact_LCH[2] = atoi (&name[1]);
      exact_LCH[0] = atoi (strchr(name, 'L')+1);
      exact_LCH[1] = atoi (strchr(name, 'C')+1);

      coloritto_ensure (color, COLORITTO_LAB);

      babl_process (babl_fish (babl_format ("CIE LCH(ab) float"),
                               babl_format ("CIE Lab float")),
                    exact_LCH, exact_Lab, 1);

      //fprintf (stderr, "%s\n", name);
      //fprintf (stderr, "%f %f %f\n", exact_LCH[2], exact_LCH[0], exact_LCH[1]);
      //  fprintf (stderr, "%f %f %f\n", exact_LCH[2], exact_LCH[0], exact_LCH[1]);

#define sq(a) ((a)*(a))
      color->delta_E = sqrt ( sq(color->lab[0]-exact_Lab[0]) +
                       sq(color->lab[1]-exact_Lab[1]) +
                       sq(color->lab[2]-exact_Lab[2]));

#undef sq
      total ++;
      sum_delta_E += color->delta_E;
      if (color->delta_E < min_delta_E)
        min_delta_E = color->delta_E;
      if (color->delta_E > max_delta_E)
      {
        max_delta_E = color->delta_E;
        max_no = i;
      }
      //fprintf (stderr, "%f\n", delta_E);
    }
  }

  fprintf (stderr, "%i-%i %inm ", LUZ_SPECTRUM_START, LUZ_SPECTRUM_END, LUZ_SPECTRUM_GAP);
  //fprintf (stderr, "%i colors\n", total);
  fprintf (stderr, "ΔE*  avg:%f", sum_delta_E / total);
  fprintf (stderr, "  min: %f", min_delta_E);

  if (max_no){
    Coloritto *color = coloritto_collection_get_no (cc, max_no);
    float triplet[3];
  fprintf (stderr, "  max: %f", max_delta_E);

  luz_list_sort (&cc->list, compare_delta_e, NULL);
  fprintf (stderr, "\n");

  int no = 0;
   for (LuzList *l = cc->list; l && no < 10; l = l->next, no++)
   {
      color = l->data;
      coloritto_get (color, COLORITTO_LCH, triplet);
      fprintf (stderr, "%s ΔE* %f   LCH: %2.4f %2.4f %2.4f\n",
           coloritto_get_name (color), color->delta_E,
           triplet[2], triplet[0], triplet[1]);
   }

  }
}


char *
coloritto_collection_to_text (ColorittoCollection *cc)
{
  LuzString *str = luz_string_new ("");

  luz_string_append_printf (str, "\n\n");
if (1){
  const char **keys = coloritto_collection_get_keys (cc);
  for (int k = 0; keys[k]; k++)
    {
      luz_string_append_printf (str, "%s=%s\n", keys[k], coloritto_collection_get_key (cc, keys[k]));
    }
  luz_string_append_printf (str, "\n");
}

  int count = coloritto_collection_count (cc);
  for (int i = 1; i <= count; i++)
  {
    float triplet[3];
    luz_string_append_printf (str, "%3i:", i);

    Coloritto *color = coloritto_collection_get_no (cc, i);
    const char **keys = coloritto_get_keys (color);
    for (int k = 0; keys[k]; k++)
    {
      luz_string_append_printf (str, " %s=%s", keys[k], coloritto_get_key (color, keys[k]));
    }

    luz_string_append_printf (str, "\n");
#define colcond(col) \
       ((print_spaces & col) || \
        ((!print_spaces || (print_spaces & COLORITTO_SET))\
        && coloritto_is_set (color, col)))

    if colcond(COLORITTO_LAB)
    {
      coloritto_ensure (color, COLORITTO_LAB);
      luz_string_append_printf (str, "     CIE Lab : %2.3f %2.3f %2.3f\n",  color->lab[0], color->lab[1], color->lab[2]);
    }
    if colcond(COLORITTO_XYZ)
    {
      coloritto_get (color, COLORITTO_XYZ, triplet);
      luz_string_append_printf (str, "     CIE XYZ : %2.3f %2.3f %2.3f\n", triplet[0], triplet[1], triplet[2]);
    }
    if colcond(COLORITTO_LCH)
    {
      coloritto_get (color, COLORITTO_LCH, triplet);
      luz_string_append_printf (str, "     CIE HLC : %2.3f %2.3f %2.3f\n",  triplet[2], triplet[0], triplet[1]);
    }
    if colcond(COLORITTO_xyY)
    {
      coloritto_ensure (color, COLORITTO_xyY);
      luz_string_append_printf (str, "     CIE xyY : %2.3f %2.3f %2.3f\n",  color->xyY[0], color->xyY[1], color->xyY[2]);
    }
    if colcond(COLORITTO_SRGB)
    {
      coloritto_ensure (color, COLORITTO_SRGB);
      luz_string_append_printf (str, "     sRGB    : %2.0f %2.0f %2.0f\n",  round(color->srgb[0]*255), round(color->srgb[1]*255), round(color->srgb[2]*255));
    }

    if colcond(COLORITTO_SPECTRUM_ON_WHITE)
    {
      coloritto_get (color, COLORITTO_LCH, triplet);
      luz_string_append_printf (str, "spectr HLC:%2.4f %2.4f %2.4f\n",  triplet[2], triplet[0], triplet[1]);
    }

  }

  return luz_string_dissolve (str);
}



static void parse_args (int argc, char **argv)
{
  for (int i = 1; argv[i]; i++)
  {
    if (!strcmp (argv[i], "--ui")) use_ui = 1;
    else if (!strcmp (argv[i], "-ui")) use_ui = 1;
    else if (!strcmp (argv[i], "-repl")) use_repl = 1;
    else if (!strcmp (argv[i], "--test-precision")) test_precision = 1;
    else if (!strcmp (argv[i], "-c")){
      if (!argv[i+1] || argv[i+1][0]=='-')
      {
        fprintf (stderr, "expected argument after -c\n");
        exit (-1);
      }
      i++;
      if (!strcmp (argv[i], "srgb"))
        print_spaces |= COLORITTO_SRGB;
      if (!strcmp (argv[i], "xyz"))
        print_spaces |= COLORITTO_XYZ;
      if (!strcmp (argv[i], "xyY"))
        print_spaces |= COLORITTO_xyY;
      if (!strcmp (argv[i], "lab"))
        print_spaces |= COLORITTO_LAB;
      if (!strcmp (argv[i], "lch"))
        print_spaces |= COLORITTO_LCH;
      if (!strcmp (argv[i], "set"))
        print_spaces |= COLORITTO_SET;
    }
    else if (argv[i][0]=='-')
    {
      fprintf (stderr, "unknown option '%s' recognized -c <srg|xyz|xyZ|lab|lcg|set>  --ui and --test-precision  -\n", argv[i]);
      exit (-1);
    }
    else
    {
      ColorittoCollection *cc =
        coloritto_collection_new_from_cxf_path (argv[i]);
      if (cc)
      {
        coloritto_collection_set_key (cc, "filename", basename (argv[1]));
        luz_list_append (&palettes, cc);
      }
    }
  }
}

int
main (int argc, char **argv)
{
  init_luz ();
  babl_init  ();

  commandline = strdup ("");
  add_items ();

  display_rgbf = babl_format ("R'G'B' float");

  parse_args (argc, argv);

#if HAVE_MRG
  if (use_ui)
  {
    int   length = 0;
    const char *profile;

    mrg  = mrg_new (1400, 800, NULL);
    mrg_set_title (mrg, "Coloritto");
    /* attempt to fetch ICC profile */
    profile  = (void*)mrg_get_profile (mrg, &length);
    if (profile)
      coloritto_set_display_icc (profile, length);

    if (luz_list_length (palettes) == 1)
    {
      argvs_eval ("pal-to-set");
    }
    argvs_eval ("lab");

    mrg_set_ui (mrg, ui, NULL);
    mrg_main (mrg);
  }
  else
#endif
  if (use_repl)
  {
    argvs_eval ("argvs");
  }
  else if (test_precision)
  {
    LuzList *l;
    for (l = palettes; l; l = l->next)
    {
      ColorittoCollection *cc = l->data;
      analyze_precision (cc);
    }
  }
  else
  {
    LuzList *l;
    for (l = palettes; l; l = l->next)
    {
      ColorittoCollection *cc = l->data;
      fprintf (stdout, "%s", coloritto_collection_to_text (cc));
    }
  }
  return 0;
}

Coloritto *clipboard = NULL; // hacky to let us implement a quick paste..


int cmd_pal_to_set (COMMAND_ARGS) /* "pal-to-set", 0, "", "adds all colors of palette to 3d view" */
{
  ColorittoCollection *cc = list_nth_data (palettes, palette_no);
  LuzList *palette = cc->list;
  for (LuzList *l = palette; l; l = l->next)
  {
    ColorLemma *item = calloc (sizeof (ColorLemma), 1);
    item->color = coloritto_ref (l->data);
    item->radius = 0.004;
    luz_list_prepend (&items, item);
  }
  return 0;
}

int cmd_remove (COMMAND_ARGS) /* "pal-remove", 0, "[entry no]", "removes a palette entry no or pal_no if none is selected" */
{
  ColorittoCollection *cc = list_nth_data (palettes, palette_no);
  LuzList *palette = cc->list;
  int rno = pal_no;
  if (argv[1])
  {
    rno = atoi(argv[1]);
  }
  int no = 0;
  for (LuzList *l = palette; l; l = l->next, no ++)
  {
    if (no == rno)
    {
      clipboard = l->data;
      luz_list_remove (&palette, l->data);
      break;
    }
  }

  update_current ();
#if HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

int cmd_copy (COMMAND_ARGS) /* "pal-copy", 0, "[entry no]", "copies current pal entry or the specified one, to clipboard" */
{
  ColorittoCollection *cc = list_nth_data (palettes, palette_no);
  LuzList *palette = cc->list;
  int rno = pal_no;
  if (argv[1])
  {
    rno = atoi(argv[1]);
  }
  int no = 0;
  for (LuzList *l = palette; l; l = l->next, no ++)
  {
    if (no == rno)
    {
      clipboard = l->data;
      return 0;
    }
  }
#if HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

int cmd_pal_paste (COMMAND_ARGS) /* "pal-paste", 0, "[entry no]", "pastes current clipboard contents to specified entry no position shifting current item there down - or pastes after the current pal_no entry" */
{
  ColorittoCollection *cc = list_nth_data (palettes, palette_no);
  LuzList *palette = cc->list;
  int rno = pal_no;
  if (!clipboard) return -1;
  if (argv[1])
  {
    rno = atoi(argv[1]);
  }
  int no = 0;
  for (LuzList *l = palette; l; l = l->next, no ++)
  {
    if (no == rno)
    {
      if (l->next)
        luz_list_insert_before (&palette, l->next, clipboard);
      else
        luz_list_append (&palette, clipboard);
#if HAVE_MRG
      mrg_queue_draw (mrg, NULL);
#endif
      return 0;
    }
  }
  luz_list_append (&palette, clipboard);

#if HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}



int cmd_pal (COMMAND_ARGS) /* "pal", 0, "[entry no]", "sets which palette entry no is current" */
{
  if (argv[1])
  {
    pal_no = atoi(argv[1]);
  }
  else
    pal_no = 0;
  update_current ();
#if HAVE_MRG
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

int cmd_pal_prev (COMMAND_ARGS) /* "pal-prev", 0, "", "make previous palette entry active" */
{
  pal_no --;
  if (pal_no < 0)
    pal_no = 0;
  update_current ();
  return 0;
}

int cmd_pal_next (COMMAND_ARGS) /* "pal-next", 0, "", "make next palette entry active" */
{
  pal_no ++;
  update_current ();
  return 0;
}

int cmd_clear (COMMAND_ARGS) /* "clear", 0, "", "clear scrollback" */
{
#if HAVE_MRG
  while (scrollback)
  {
    char *data = scrollback->data;
    mrg_list_remove (&scrollback, data);
    free (data);
  }
  mrg_queue_draw (mrg, NULL);
#endif
  return 0;
}

int cmd_quit (COMMAND_ARGS) /* "quit", 0, "", "exit" */
{
#if HAVE_MRG
  mrg_quit (mrg);
#endif
  return 0;
}

int cmd_exit (COMMAND_ARGS) /* "q", 0, "", "exit" */
{
#if HAVE_MRG
  mrg_quit (mrg);
#endif
  return 0;
}


ColorittoCollection *
coloritto_collection_new (void)
{
  ColorittoCollection *cc = calloc (sizeof (ColorittoCollection), 1);
  return cc;
}

void
coloritto_collection_destroy (ColorittoCollection *cc)
{
}


void
coloritto_collection_load_cxf (ColorittoCollection *cc,
                               const char *cxf_data)
{
  const char *contents = cxf_data;
  int pos = 0;
  LuzXml *xmltok;
  float lab[3]={0,0,0};
  float srgb[3]={0,0,0};
  float xyz[3]={0,0,0};
  float spectrum[LUZ_SPECTRUM_BANDS]={0,0,0};
  float wavelength, prev_wavelength, first_wavelength;
  float start_wl = 380.0;
  float increment = 10.0;

  /* and a final pass for composite of custom items
     if we only ever run a single instance at a same time then
     we can pollute the global id name space with temporarily
     loaded spectrums from the first pass, simplifying architecture,
     the architecture should grow a global repository that exists in
     addition to the local and with time temporary - set of colorittos

     each coloritto should be able to be fully self contained, with a
     core unit and a variable sized side unit
  */

  XmlState state = IN_NONE;

  // XXX : all these are leaked
  LuzString *colorsetname = luz_string_new ("");
  LuzString *description = luz_string_new ("");
  LuzString *colordepth = luz_string_new ("");
  LuzString *attname = luz_string_new ("");
  LuzString *illuminant = luz_string_new ("");
  LuzString *reflectance_spectrum = luz_string_new ("");

  Coloritto *color = NULL;

  int type = t_none;
  first_wavelength = 0;

  xmltok = xmltok_buf_new ((void*)contents);

  while (type != t_eof)
  {
    char *data = NULL;
    type = xmltok_get (xmltok, &data, &pos);
    switch (type)
    {
      case t_entity:
      case t_word:
        switch (state)
          {
            case IN_COLOR_DEPTH_TAG:
                luz_string_set (colordepth, data);
            break;
            case IN_ILLUMINANT_TAG:
                luz_string_set (illuminant, data);
                break;
            case IN_DESCRIPTION_TAG:
                if (description->str[0])
                  luz_string_append_str (description, " ");
                luz_string_append_str (description, data);
                break;
            default:
              case IN_NONE:
                do{}while(0);
          }
         break;
      case t_whitespace:
        {
          //fprintf (stderr, "%s", data);
        }
        break;

      case t_tag:
        {
          if (has_suffix (data, ":Illuminant"))
             state = IN_ILLUMINANT_TAG;
          else if (has_suffix (data, ":Description"))
             state = IN_DESCRIPTION_TAG;
          else if (has_suffix (data, ":ColorDepth"))
             state = IN_COLOR_DEPTH_TAG;
          else
             state = IN_NONE;
        }
        break;

      case t_closetag:
        if (has_suffix (data, ":Description"))
        {
            coloritto_collection_set_key (cc, "name", description->str);
        }
        break;

      case t_att:
        {
          //fprintf (stderr, "att:%s ", data);
          luz_string_set (attname, data);
        }
        break;
      case t_val:
        {
          if (!strcmp ("StartWL", attname->str))
          {
            start_wl = strtod (data, NULL);
          }
          else if (!strcmp ("Increment", attname->str))
          {
            increment = strtod (data, NULL);
          }
        }
        break;
    }
  }

  xmltok = xmltok_buf_new ((void*)contents);
  state = IN_NONE;
  type = t_none;

  while (type != t_eof)
  {
    char *data = NULL;
    type = xmltok_get (xmltok, &data, &pos);
    switch (type)
    {
      case t_entity:
        {
          fprintf (stderr, "ent: %s\n", data);
        }
        break;
      case t_word:
        switch (state)
          {
            case IN_XYZ_X_TAG: xyz[0] = strtod (data, NULL);

            break;
            case IN_XYZ_Y_TAG: xyz[1] = strtod (data, NULL);

            break;
            case IN_XYZ_Z_TAG: xyz[2] = strtod (data, NULL);
             coloritto_set (color, COLORITTO_SRGB, &xyz[0]);
            break;

            case IN_LAB_L_TAG: lab[0] = strtod (data, NULL); break;
            case IN_LAB_A_TAG: lab[1] = strtod (data, NULL); break;
            case IN_LAB_B_TAG: lab[2] = strtod (data, NULL);
               coloritto_set (color, COLORITTO_LAB, &lab[0]);
               break;

            case IN_SRGB_RED_TAG:   srgb[0] = strtod (data, NULL); break;
            case IN_SRGB_GREEN_TAG: srgb[1] = strtod (data, NULL); break;
            case IN_SRGB_BLUE_TAG:  srgb[2] = strtod (data, NULL);
            coloritto_set (color, COLORITTO_SRGB, &srgb[0]);
                break;

            case IN_REFLECTANCE_POINT_TAG: /* XXX: tune */
               spectrum[0] = strtod (data, NULL); 

            case IN_REFLECTANCE_SPECTRUM_TAG: /* XXX: tune */
               luz_string_append_str (reflectance_spectrum, data);
               luz_string_append_str (reflectance_spectrum, " ");
             break;

            case IN_COLOR_DEPTH_TAG: luz_string_set (colordepth, data); break;
            case IN_ILLUMINANT_TAG: luz_string_set (illuminant, data); break;
            case IN_DESCRIPTION_TAG:
               if (description->str[0])
                 luz_string_append_str (description, " ");
               luz_string_append_str (description, data);
               break;
            default:
              case IN_NONE:
                do{}while(0);
          }
         break;
      case t_whitespace:
        {
          //fprintf (stderr, "%s", data);
        }
        break;

      case t_tag:
        {
          //fprintf (stderr, "tag: %s ", data);
          if (has_suffix (data, ":Color") ||
              has_suffix (data, ":Object")) {
                 state = IN_NONE;
                 color = coloritto_new ();
                 coloritto_collection_set_no (cc, -1, color);
          }
          else if (has_suffix (data, ":ColorSpaceCIEXYZ"))    state = IN_XYZ_TAG;
          else if (has_suffix (data, ":X"))    state = IN_XYZ_X_TAG;
          else if (has_suffix (data, ":Y"))    state = IN_XYZ_Y_TAG;
          else if (has_suffix (data, ":Z"))    state = IN_XYZ_Z_TAG;
          else if (has_suffix (data, ":ColorSpaceCIELab"))   state = IN_LAB_TAG;
          else if (has_suffix (data, ":ColorSpaceSRGB"))     state = IN_SRGB_TAG;
          else if (has_suffix (data, ":ColorSpaceSpectral")) state = IN_SPECTRAL_TAG;
          else if (has_suffix (data, ":Red"))                state = IN_SRGB_RED_TAG;
          else if (has_suffix (data, ":Green"))              state = IN_SRGB_GREEN_TAG;
          else if (has_suffix (data, ":Blue"))               state = IN_SRGB_BLUE_TAG;
          else if (has_suffix (data, ":L"))                  state = IN_LAB_L_TAG;
          else if (has_suffix (data, ":A"))                  state = IN_LAB_A_TAG;
          else if (has_suffix (data, ":B"))                  state = IN_LAB_B_TAG;
          else if (has_suffix (data, ":Illuminant"))         state = IN_ILLUMINANT_TAG;
          else if (has_suffix (data, ":Description"))        state = IN_DESCRIPTION_TAG;
          else if (has_suffix (data, ":ReflectancePoint")) {  state = IN_REFLECTANCE_POINT_TAG; }
          else if (has_suffix (data, ":ReflectanceSpectrum")) state = IN_REFLECTANCE_SPECTRUM_TAG;
          else if (has_suffix (data, ":ColorDepth"))         state = IN_COLOR_DEPTH_TAG;
        }
        break;

      case t_closetag:
        if (has_suffix (data, ":Description"))
        {
            coloritto_collection_set_key (cc, "name", description->str);
        }
        else if (has_suffix (data, ":ColorSpaceSpectral"))
        {
          //fprintf (stderr, "fnord! %s %f %f [%s]\n", colorname->str, start_wl, increment, reflectance_spectrum->str);


          Spectrum spec;
          LuzString *str = luz_string_new ("");
          luz_string_append_printf (str, "%f %f 1 %s", start_wl, increment, reflectance_spectrum->str);
          spec = luz_parse_spectrum (luz, str->str);
          luz_string_free (str, 1);

          coloritto_set (color,  COLORITTO_SPECTRUM_ON_WHITE, (void*)&spec);
        }
        if (has_suffix (data, ":ReflectanceSpectrum"))
        {
          Spectrum spec;
          LuzString *str = luz_string_new ("");
          luz_string_append_printf (str, "%f %f 1 %s", start_wl, increment, reflectance_spectrum->str);
          spec = luz_parse_spectrum (luz, str->str);
          luz_string_free (str, 1);
          coloritto_set (color,  COLORITTO_SPECTRUM_ON_WHITE, (void*)&spec);

        }
        if (has_suffix (data, ":Color"))
// || has_suffix (data, ":ColorSpaceSRGB") || has_suffix (data, ":ColorCIEXYZ"))
        {
        }

        state = IN_NONE;
        break;

      case t_att:
        {
          //fprintf (stderr, "att:%s ", data);
          luz_string_set (attname, data);
        }
        break;
      case t_val:
        {

          if (!strcmp ("PaletteName", attname->str))
          {
            coloritto_collection_set_key (cc, "name", data);
          }
          else if (!strcmp ("Name", attname->str))
          {
            if (color) coloritto_set_key (color, "name", data);
            luz_string_set (attname, "");
            luz_string_set (reflectance_spectrum, "");
          }
          else if (!strcmp ("ColorName", attname->str))
          {
            coloritto_set_key (color, "name", data);
            luz_string_set (attname, "");
            luz_string_set (reflectance_spectrum, "");
          }
          else if (!strcmp ("Wavelength", attname->str))
          {
            prev_wavelength = wavelength;
            wavelength = strtod (data, NULL);
            if (first_wavelength == 0){
              first_wavelength = wavelength;
              start_wl = wavelength;
            }
            increment = wavelength - prev_wavelength;
          }
          else if (!strcmp ("StartWL", attname->str))
          {
            start_wl = strtod (data, NULL);
          }
          else if (!strcmp ("Increment", attname->str))
          {
            increment = strtod (data, NULL);
          }
          else if (!strcmp ("ColorSetName", attname->str))
          {
            luz_string_set (colorsetname, data);
            luz_string_set (attname, "");
            luz_string_set (colordepth, "");
          }
          else
          {
            //fprintf (stderr, "val:<<<%s>>> ", data);
          }
        }
        break;
    }
  }
}

ColorittoCollection *
coloritto_collection_new_from_cxf (const char *cxf_data)
{
  ColorittoCollection *cc = coloritto_collection_new ();
  coloritto_collection_load_cxf (cc, cxf_data);
  return cc;
}

ColorittoCollection * coloritto_collection_new_from_cxf_path (const char *cxf_path)
{
  long length;
  char *contents = NULL;
  file_get_contents (cxf_path, &contents, &length);
  if (contents)
  {
    ColorittoCollection *cc = coloritto_collection_new_from_cxf (contents);
    free (contents);
    return cc;
  }
  return NULL;
}

int
coloritto_collection_count (ColorittoCollection *cc)
{
  return luz_list_length (cc->list);
}

Coloritto *
coloritto_collection_get_no (ColorittoCollection *cc,
                             int                  no)
{
  LuzList *l = luz_list_nth (cc->list, no - 1);
  if (l) return l->data;
  return NULL;
}

void
coloritto_collection_set_no (ColorittoCollection *cc,
                             int                  rno,
                             Coloritto           *coloritto)
{
  int no = 0;
  for (LuzList *l = cc->list; l; l = l->next, no++)
  {
    if (no == rno - 1)
    {
      coloritto_unref (l->data); // we should ref count colorittos?
      l->data = coloritto;
      return;
    }
  }
  luz_list_append (&cc->list, coloritto);
}

void
coloritto_collection_insert_no (ColorittoCollection *cc,
                                int                  rno,
                                Coloritto           *coloritto)
{
  int no = 0;
  if (rno > 0)
  for (LuzList *l = cc->list; l; l = l->next, no++)
  {
    if (no == rno - 1)
    {
      if (l->next)
        luz_list_insert_before (&cc->list, l->next, coloritto);
      else
        luz_list_append (&cc->list, coloritto);
      return;
    }
  }
  luz_list_append (&cc->list, coloritto);
}

void
coloritto_collection_remove_no (ColorittoCollection *cc,
                                int                  rno)
{
  int no = 0;
  for (LuzList *l = cc->list; l; l = l->next, no ++)
  {
    if (no == rno - 1)
    {
      luz_list_remove (&cc->list, l->data);
      return;
    }
  }
}

#include <signal.h>

void
coloritto_collection_set_key  (ColorittoCollection  *cc,
                               const char *key,
                               const char *value)
{
  int i;
  for (i = 0; i < cc->kvs; i++)
    if (cc->keys[i] && !strcmp (key, cc->keys[i]))
    {
      free (cc->values[i]);
      cc->values[i] = strdup (value);
      return;
    }

  if (cc->kvs >= MAX_KV)
  {
    fprintf (stderr, "key value count overflow\n");
    return;
  }
  cc->keys[cc->kvs]   = strdup (key);
  cc->values[cc->kvs] = strdup (value);

  //raise(SIGINT);

  cc->kvs++;
  cc->keys[cc->kvs] = NULL;
}

const char *
coloritto_collection_get_key (ColorittoCollection *cc,
                              const char          *key)
{
  for (int i = 0; i < cc->kvs; i++)
    if (cc->keys[i] && !strcmp (key, cc->keys[i]))
    {
      return cc->values[i];
    }
  return NULL;
}

const char **coloritto_get_keys (Coloritto *color)
{
  return (void*)&color->keys[0];
}

const char **coloritto_collection_get_keys (ColorittoCollection *cc)
{
  return (void*)&cc->keys[0];
}
