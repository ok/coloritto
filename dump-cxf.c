#include "luz-xml.h"
#include <string.h>
#include <stdlib.h>

static int
file_get_contents (const char  *path,
                   char       **contents,
                   long        *length)
{
  FILE *file;
  long  size;
  long  remaining;
  char *buffer;

  file = fopen (path, "rb");

  if (!file)
    return -1;

  fseek (file, 0, SEEK_END);
  *length = size = remaining = ftell (file);
  rewind (file);
  buffer = malloc(size + 8);

  if (!buffer)
    {
      fclose(file);
      return -1;
    }

  remaining -= fread (buffer, 1, remaining, file);
  if (remaining)
    {
      fclose (file);
      free (buffer);
      return -1;
    }
  fclose (file);
  *contents = buffer;
  buffer[size] = 0;
  return 0;
}


int dump_cxf_main (int argc, char **argv)
{
  char *contents = NULL;
  int pos = 0;
  long length;
  LuzXml *xmltok;
  int type = t_none;
  file_get_contents (argv[1], &contents, &length);
  xmltok = xmltok_buf_new (contents);
  
  while (type != t_eof)
  {
    char *data = NULL;
    type = xmltok_get (xmltok, &data, &pos);
    switch (type)
    {
      case t_entity:
        {
          fprintf (stderr, "ent: %s\n", data);
        }
        break;
      case t_word:
      case t_whitespace:
        {
          fprintf (stderr, "%s", data);
        }
        break;

      case t_tag:
        {
          fprintf (stderr, " tag:%s", data);
        }
        break;

      case t_att:
        {
          fprintf (stderr, " att:%s", data);
        }
        break;
      case t_val:
        {
          fprintf (stderr, " val:%s", data);
        }
        break;
    }
  }

  return 0;
}
