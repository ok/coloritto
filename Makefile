all: coloritto-cli coloritto

coloritto-cli: *.c argvs-commands.inc *.h *.inc Makefile
	ccache gcc *.c -o coloritto-cli -lm `pkg-config babl --cflags --libs` -Wall -Wno-unused-but-set-variable -g

coloritto: *.c argvs-commands.inc *.h *.inc Makefile
	ccache gcc *.c -o coloritto -DHAVE_MRG -lm `pkg-config mrg babl --cflags --libs` -Wall -Wno-unused-but-set-variable -g
clean:
	rm -f coloritto coloritto-cli argvs-commands.inc

argvs-commands.inc: *.c Makefile
	@echo " GEN" $@ 
	@./argvs_extract.sh *.c > argvs-commands.inc

