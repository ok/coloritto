/* luz - MicroRaptor Gui
 * Copyright (c) 2014 Øyvind Kolås <pippin@hodefoting.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LUZ_LIST__
#define  __LUZ_LIST__

#include <stdlib.h>

/* The whole luz_list implementation is in the header and will be inlined
 * wherever it is used.
 */

typedef struct _LuzList LuzList;
  struct _LuzList {void *data;LuzList *next;
  void (*freefunc)(void *data, void *freefunc_data);
  void *freefunc_data;
}
;

static inline void luz_list_prepend_full (LuzList **list, void *data,
    void (*freefunc)(void *data, void *freefunc_data),
    void *freefunc_data)
{
  LuzList *new_=calloc (sizeof (LuzList), 1);
  new_->next=*list;
  new_->data=data;
  new_->freefunc=freefunc;
  new_->freefunc_data = freefunc_data;
  *list = new_;
}

static inline int luz_list_length (LuzList *list)
{
  int length = 0;
  LuzList *l;
  for (l = list; l; l = l->next, length++);
  return length;
}

static inline void luz_list_prepend (LuzList **list, void *data)
{
  LuzList *new_=calloc (sizeof (LuzList), 1);
  new_->next= *list;
  new_->data=data;
  *list = new_;
}

static inline void *luz_list_last (LuzList *list)
{
  if (list)
    {
      LuzList *last;
      for (last = list; last->next; last=last->next);
      return last->data;
    }
  return NULL;
}

static inline void luz_list_append_full (LuzList **list, void *data,
    void (*freefunc)(void *data, void *freefunc_data),
    void *freefunc_data)
{
  LuzList *new_= calloc (sizeof (LuzList), 1);
  new_->data=data;
  new_->freefunc = freefunc;
  new_->freefunc_data = freefunc_data;
  if (*list)
    {
      LuzList *last;
      for (last = *list; last->next; last=last->next);
      last->next = new_;
      return;
    }
  *list = new_;
  return;
}

static inline void luz_list_append (LuzList **list, void *data)
{
  luz_list_append_full (list, data, NULL, NULL);
}

static inline void luz_list_remove (LuzList **list, void *data)
{
  LuzList *iter, *prev = NULL;
  if ((*list)->data == data)
    {
      if ((*list)->freefunc)
        (*list)->freefunc ((*list)->data, (*list)->freefunc_data);
      prev = (void*)(*list)->next;
      free (*list);
      *list = prev;
      return;
    }
  for (iter = *list; iter; iter = iter->next)
    if (iter->data == data)
      {
        if (iter->freefunc)
          iter->freefunc (iter->data, iter->freefunc_data);
        prev->next = iter->next;
        free (iter);
        break;
      }
    else
      prev = iter;
}

static inline void luz_list_free (LuzList **list)
{
  while (*list)
    luz_list_remove (list, (*list)->data);
}

static inline LuzList *luz_list_nth (LuzList *list, int no)
{
  while(no-- && list)
    list = list->next;
  return list;
}

static inline LuzList *luz_list_find (LuzList *list, void *data)
{
  for (;list;list=list->next)
    if (list->data == data)
      break;
  return list;
}

void luz_list_sort (LuzList **list,
    int(*compare)(const void *a, const void *b, void *userdata),
    void *userdata);

static inline void
luz_list_insert_before (LuzList **list, LuzList *sibling,
                        void *data)
{
  if (*list == NULL || *list == sibling)
    {
      luz_list_prepend (list, data);
    }
  else
    {
      LuzList *prev = NULL;
      for (LuzList *l = *list; l; l=l->next)
        {
          if (l == sibling)
            break;
          prev = l;
        }
      if (prev) {
        LuzList *new_=calloc(sizeof (LuzList), 1);
        new_->next = sibling;
        new_->data = data;
        prev->next=new_;
      }
    }
}

static inline void
luz_list_insert_sorted (LuzList **list, void *data,
                       int(*compare)(const void *a, const void *b, void *userdata),
                       void *userdata)
{
  luz_list_prepend (list, data);
  luz_list_sort (list, compare, userdata);
}

static inline void
luz_list_reverse (LuzList **list)
{
  LuzList *new_ = NULL;
  LuzList *l;
  for (l = *list; l; l=l->next)
    luz_list_prepend (&new_, l->data);
  luz_list_free (list);
  *list = new_;
}

#endif
