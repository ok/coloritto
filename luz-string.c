/* luz - MicroRaptor Gui
 * Copyright (c) 2014 Øyvind Kolås <pippin@hodefoting.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "luz-string.h"
#include "luz-utf8.h"

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void luz_string_init (LuzString *string, int initial_size)
{
  string->allocated_length = initial_size;
  string->length = 0;
  string->utf8_length = 0;
  string->str = malloc (string->allocated_length);
  string->str[0]='\0';
}

static void luz_string_destroy (LuzString *string)
{
  if (string->str)
  {
    free (string->str);
    string->str = NULL;
  }
}

void luz_string_clear (LuzString *string)
{
  string->length = 0;
  string->utf8_length = 0;
  string->str[string->length]=0;
}

static inline void _luz_string_append_byte (LuzString *string, char  val)
{
  if ((val & 0xC0) != 0x80)
    string->utf8_length++;
  if (string->length + 1 >= string->allocated_length)
    {
      char *old = string->str;
      string->allocated_length *= 2;
      string->str = malloc (string->allocated_length);
      memcpy (string->str, old, string->allocated_length/2);
      free (old);
    }
  string->str[string->length++] = val;
  string->str[string->length] = '\0';
}
void luz_string_append_byte (LuzString *string, char  val)
{
  _luz_string_append_byte (string, val);
}

void luz_string_append_unichar (LuzString *string, unsigned int unichar)
{
  char *str;
  char utf8[5];
  utf8[luz_unichar_to_utf8 (unichar, (unsigned char*)utf8)]=0;
  str = utf8;

  while (str && *str)
    {
      _luz_string_append_byte (string, *str);
      str++;
    }
}

static inline void _luz_string_append_str (LuzString *string, const char *str)
{
  if (!str) return;
  while (*str)
    {
      _luz_string_append_byte (string, *str);
      str++;
    }
}
void luz_string_append_str (LuzString *string, const char *str)
{
  _luz_string_append_str (string, str);
}

LuzString *luz_string_new_with_size (const char *initial, int initial_size)
{
  LuzString *string = calloc (sizeof (LuzString), 1);
  luz_string_init (string, initial_size);
  if (initial)
    _luz_string_append_str (string, initial);
  return string;
}

LuzString *luz_string_new (const char *initial)
{
  return luz_string_new_with_size (initial, 8);
}

void luz_string_append_data (LuzString *string, const char *str, int len)
{
  int i;
  for (i = 0; i<len; i++)
    _luz_string_append_byte (string, str[i]);
}

void luz_string_append_string (LuzString *string, LuzString *string2)
{
  const char *str = luz_string_get (string2);
  while (str && *str)
    {
      _luz_string_append_byte (string, *str);
      str++;
    }
}

const char *luz_string_get (LuzString *string)
{
  return string->str;
}

int luz_string_get_length (LuzString *string)
{
  return string->length;
}

/* dissolving a string, means destroying it, but returning
 * the string, that should be manually freed.
 */
char *luz_string_dissolve   (LuzString *string)
{
  char *ret = string->str;
  string->str = NULL;
  free (string);
  return ret;
}

void
luz_string_free (LuzString *string, int freealloc)
{
  if (freealloc)
    {
      luz_string_destroy (string);
    }
  free (string);
}

void
luz_string_append_printf (LuzString *string, const char *format, ...)
{
  va_list ap;
  size_t needed;
  char  *buffer;
  va_start(ap, format);
  needed = vsnprintf(NULL, 0, format, ap) + 1;
  buffer = malloc(needed);
  va_end (ap);
  va_start(ap, format);
  vsnprintf(buffer, needed, format, ap);
  va_end (ap);
  _luz_string_append_str (string, buffer);
  free (buffer);
}

void
luz_string_set (LuzString *string, const char *new_string)
{
  luz_string_clear (string);
  _luz_string_append_str (string, new_string);
}

#if 0
#include "luz-list.h"
static LuzList *interns = NULL;

const char * luz_intern_string (const char *str)
{
  LuzList *i;
  for (i = interns; i; i = i->next)
  {
    if (!strcmp (i->data, str))
      return i->data;
  }
  str = strdup (str);
  luz_list_append (&interns, (void*)str);
  return str;
}
#endif

void luz_string_replace_utf8 (LuzString *string, int pos, const char *new_glyph)
{
  int new_len = luz_utf8_len (*new_glyph);
  int old_len = string->utf8_length;
  char tmpg[3]=" ";
  if (new_len <= 1 && new_glyph[0] < 32)
  {
    tmpg[0]=new_glyph[0]+64;
    new_glyph = tmpg;
  }

  if (pos == old_len)
  {
    _luz_string_append_str (string, new_glyph);
    return;
  }

  {
    for (int i = old_len; i <= pos; i++)
    {
      _luz_string_append_byte (string, ' ');
      old_len++;
    }
  }

  if (string->length + new_len  > string->allocated_length)
  {
    char *tmp;
    char *defer;
    string->allocated_length = string->length + new_len;
    tmp = calloc (string->allocated_length, 1);
    strcpy (tmp, string->str);
    defer = string->str;
    string->str = tmp;
    free (defer);
  }

  char *p = (void*)luz_utf8_skip (string->str, pos);
  int prev_len = luz_utf8_len (*p);
  char *rest;
  if (*p == 0 || *(p+prev_len) == 0)
  {
    rest = strdup("");
  }
  else
  {
    rest = strdup (p + prev_len);
  }

  memcpy (p, new_glyph, new_len);
  memcpy (p + new_len, rest, strlen (rest) + 1);
  string->length += new_len;
  string->length -= prev_len;
  free (rest);

  string->utf8_length = luz_utf8_strlen (string->str);
}

void luz_string_insert_utf8 (LuzString *string, int pos, const char *new_glyph)
{
  int new_len = luz_utf8_len (*new_glyph);
  int old_len = string->utf8_length;
  char tmpg[3]=" ";
  if (new_len <= 1 && new_glyph[0] < 32)
  {
    tmpg[0]=new_glyph[0]+64;
    new_glyph = tmpg;
  }

  if (pos == old_len)
  {
    _luz_string_append_str (string, new_glyph);
    return;
  }

  {
    for (int i = old_len; i <= pos; i++)
    {
      _luz_string_append_byte (string, ' ');
      old_len++;
    }
  }

  if (string->length + new_len + 1  > string->allocated_length)
  {
    char *tmp;
    char *defer;
    string->allocated_length = string->length + new_len + 1;
    tmp = calloc (string->allocated_length, 1);
    strcpy (tmp, string->str);
    defer = string->str;
    string->str = tmp;
    free (defer);
  }

  char *p = (void*)luz_utf8_skip (string->str, pos);
  int prev_len = luz_utf8_len (*p);
  char *rest;
  if (*p == 0 || *(p+prev_len) == 0)
  {
    rest = strdup("");
  }
  else
  {
    rest = strdup (p);
  }

  memcpy (p, new_glyph, new_len);
  memcpy (p + new_len, rest, strlen (rest) + 1);
  string->length += new_len;
  free (rest);

  string->utf8_length = luz_utf8_strlen (string->str);
}

int luz_string_get_utf8_length (LuzString  *string)
{
  //return luz_utf8_strlen (string->str);
  return string->utf8_length;
}

void luz_string_remove_utf8 (LuzString *string, int pos)
{
  int old_len = string->utf8_length;

  {
    for (int i = old_len; i <= pos; i++)
    {
      _luz_string_append_byte (string, ' ');
      old_len++;
    }
  }

  char *p = (void*)luz_utf8_skip (string->str, pos);
  int prev_len = luz_utf8_len (*p);
  char *rest;
  if (*p == 0 || *(p+prev_len) == 0)
  {
    rest = strdup("");
  }
  else
  {
    rest = strdup (p + prev_len);
  }

  memcpy (p, rest, strlen (rest) + 1);
  string->length -= prev_len;
  free (rest);

  string->utf8_length = luz_utf8_strlen (string->str);
}

