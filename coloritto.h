#ifndef COLORITTO_H
#define COLORITTO_H

typedef enum ColorittoDef {
  COLORITTO_SCALAR                           = 1 << 0,
  COLORITTO_LAB                              = 1 << 1,
  COLORITTO_LCH                              = 1 << 2,
  COLORITTO_DISPLAY_RGB                      = 1 << 3,
  COLORITTO_SRGB                             = 1 << 4,
  COLORITTO_XYZ                              = 1 << 5,
  COLORITTO_xyY                              = 1 << 6,
  COLORITTO_SPECTRUM_ON_WHITE                = 1 << 7,
  COLORITTO_SPECTRUM_ON_WHITE_RAMP           = 1 << 8,
  COLORITTO_SPECTRUM_ON_WHITE_AND_BLACK      = 1 << 9,
  COLORITTO_SPECTRUM_ON_WHITE_AND_BLACK_RAMP = 1 << 10,

  COLORITTO_SET                              = 1 << 20,

} ColorittoDef;

typedef struct _Coloritto Coloritto;
typedef struct _ColorittoCollection ColorittoCollection;

Coloritto            *coloritto_new          (void);
Coloritto            *coloritto_ref          (Coloritto *color);
void                  coloritto_unref        (Coloritto *color);
void                  coloritto_destroy      (Coloritto *color);

void                  coloritto_set          (Coloritto    *color,
                                              ColorittoDef  color_type,
                                              const float  *data);

int                   coloritto_is_set       (Coloritto    *color,
                                              ColorittoDef  color_type);

void                  coloritto_get          (Coloritto   *color,
                                              ColorittoDef color_type,
                                              float       *data);

void                  coloritto_set_spectrum (Coloritto   *color,
                                              int          on_white,
                                              float        coverage,
                                              const float *spectrum);

/* interpolate? how to know which values are valid - is it relevant ? */
void                  coloritto_get_spectrum (Coloritto   *color,
                                              int          on_white,
                                              float        coverage,
                                              float       *spectrum);

float                 coloritto_get_coverage_start (Coloritto *color);
float                 coloritto_get_coverage_gap   (Coloritto *color);

void                  coloritto_set_name (Coloritto *color, const char *name);
const char           *coloritto_get_name (Coloritto *color);
void                  coloritto_set_key  (Coloritto *color, const char *key,
                                          const char *value);
const char           *coloritto_get_key  (Coloritto *color, const char *key);
const char          **coloritto_get_keys (Coloritto *color);

ColorittoCollection  *coloritto_collection_new (void);
void                  coloritto_collection_destroy (ColorittoCollection *cc);

void coloritto_collection_load_cxf (ColorittoCollection *cc, const char *cxf_data);
ColorittoCollection * coloritto_collection_new_from_cxf (const char *cxf_data);
ColorittoCollection * coloritto_collection_new_from_cxf_path (const char *cxf_path);

int                   coloritto_collection_count (ColorittoCollection *cc);

Coloritto *           coloritto_collection_get_no (ColorittoCollection *cc,
                                                   int                  no);

char *                coloritto_collection_to_cxf (ColorittoCollection *cc);
char *                coloritto_collection_to_luz (ColorittoCollection *cc);

void                  coloritto_collection_set_no (ColorittoCollection *cc,
                                                   int                  no,
                                                   Coloritto           *coloritto);
void                  coloritto_collection_insert_no (ColorittoCollection *cc,
                                                      int                  no,
                                                      Coloritto           *coloritto);
void                  coloritto_collection_remove_no (ColorittoCollection *cc,
                                                      int                  no);

void                  coloritto_collection_set_key   (ColorittoCollection *cc,
                                                      const char *key,
                                                      const char *value);
const char           *coloritto_collection_get_key   (ColorittoCollection *cc,
                                                      const char *key);
const char          **coloritto_collection_get_keys  (ColorittoCollection *cc);
void                  coloritto_set_display_icc      (const char *icc_data,
                                                      int icc_length);

#endif
