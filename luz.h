/*
 * GEGL is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2014,2016,2018,2019 Øyvind Kolås <pippin@gimp.org>
 */

/* ideas:
     allow to special case black coat in some cmyk's and use configurable
     undercolor removal in addition with a known black

     parse information for common color space info into the collection

     store attributes that can translate to 'metallicness/shine' perhaps
     a measurement at a different angle - and then interpolate between these
     two or three angles for a simplified gonometric estimate

*/

#ifndef LUZSIM_H_
#define LUZSIM_H_

#include <stdint.h>

typedef struct _Luz Luz;

#define LUZ_MAX_COATS        16

/* This defines the internal computational spectral working space of luz
 * for now it is also the range used for computing tristimulus values,
 * this should change - since it is very useful to also have values beyond
 * perceptual range for fluoresence and similar purposes.
 *
 * Initial anchoring for implementation is 360-780nm at 10nm intervals,
 * for matching representation of HLC-Colour-Atlas-XL_SpectralData_v1-2.cxf
 * and thus get a decent result with nearest-neighbor spectral resampling,
 * since our illuminants and observers are given at higher resolution in
 * internal configuration.
 */
#define LUZ_SPECTRUM_START   360
#define LUZ_SPECTRUM_GAP     10
#define LUZ_SPECTRUM_END     780 // only used on line below
#define LUZ_SPECTRUM_BANDS   (((LUZ_SPECTRUM_END-LUZ_SPECTRUM_START)/LUZ_SPECTRUM_GAP)+1)


Luz    *luz_new                (const char  *config);
void    luz_destroy            (Luz         *luz);


void    luz_coats_to_xyz       (Luz         *luz,
                                const float *coat_levels,
                                float       *xyz);
void    luz_coats_to_rgb       (Luz         *luz,
                                const float *coat_levels,
                                float       *rgb);
void    luz_rgb_to_coats       (Luz         *luz,
                                const float *rgb,
                                float       *coat_levels);
void    luz_xyz_to_coats       (Luz         *luz,
                                const float *xyz,
                                float       *coat_levels);
float   luz_get_coverage_limit (Luz         *luz);
void    luz_set_coverage_limit (Luz         *luz, float limit);
void    luz_set_coat_count     (Luz         *luz, int count);
int     luz_get_coat_count     (Luz         *luz);

typedef struct _Spectrum Spectrum;

Spectrum luz_parse_spectrum (Luz *luz, char *spectrum);
const Spectrum *luz_get_spectrum (Luz *luz, const char *name);
void            luz_set_spectrum (Luz *luz, const char *name, Spectrum *spectrum);

Spectrum luz_coats_to_spectrum  (Luz         *luz,
                                 const float *coat_levels);

Spectrum luz_rgb_to_spectrum (Luz *luz, float r, float g, float b);

void luz_spectrum_to_rgb (Luz            *luz,
                          const Spectrum *spectrum,
                          float          *rgb);

void luz_reflectance_to_xyz (Luz  *luz,
                             const Spectrum *spec,
                             float          *x,
                             float          *y,
                             float          *z);

void luz_spectrum_to_xyz (Luz            *luz,
                          const Spectrum *spectrum,
                          float          *x,
                          float          *y,
                          float          *z);

float luz_spectrum_interpolate_nm (const Spectrum *spectrum, float nm);

struct _Spectrum {
  float bands[LUZ_SPECTRUM_BANDS];
};



#endif
