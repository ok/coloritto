# Coloritto

A spectral color data collection manager, editor, and visualizer in development.
Currently under a commandline tool and a graphical browser/editor, and
eventually a library for parsing and using CXF-4 / ISO17972 files containing
spot color calibrations from other applications.

## Features

Parsing Collection name/description, color labels, CIE XYZ, CIE xyY, sRGB, CIE
Lab, CIE Lch, ICC backed display RGB, and spectral reflectances as well as on
demand conversion among all of these.

3d visualization of colors, primaries and gamuts in both perspective and
orthogonal views for both CIE xyY, CIE XYZ, CIE Lab and sRGB. Color management
of swatch rendering by using the ICC profile set in the X-server.

Further example CXF files for testing would be very welcome, the files I
currently have - from the web as well as test files references from the ISO
standard are: CGATS_Data.cxf, HLC_EPV_M0_V2-3.cxf, Holidays_test.cxf,
ISO17972-4_CxFX-4_Example_1.xml and ISO17972-4_CxFX-4_Example_2.xml I am
particularly interested in further spot-color characterisation example files.

## API

See https://gitlab.gnome.org/ok/coloritto/blob/master/coloritto.h for the
current internal draft of an API for accessing CXF-4 data.

## Commandline:

The main use of the commandline is currently to dump the datastructure as
interpreted from a file for debugging purposes.

More control over output will be added, including generating HTML and CXF
files.

```
  $ coloritto <input-cxf.xml> [-ui] [-c set] [-c srgb] [-c lch] [-c lab] [-c xyz] [-c xyY]
```

## Dependencies:

babl and mrg (http://github.com/hodefoting/mrg), the mrg dependency will in the
end not be neccesary for the coloritto library. Issuing make in the folder
should be sufficient.

## User interface and experiments

A user interface, a collection browser/editor with 3d visualization of
color-spaces has helped ensure that coloritto agrees with other softwares
interpretation of spectral data to CIE Lab mapping. This editor contains and
will improve and expose the incorporate paint/ink separation and simulation
from https://pippin.gimp.org/color-simulator/ . Possibly extending the
characterisation model to include gloss/metallicnes.

![Screenshot1](http://pippin.gimp.org/tmp/coloritto2.png)
![Screenshot2](http://pippin.gimp.org/tmp/coloritto.png)
