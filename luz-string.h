/* luz - MicroRaptor Gui
 * Copyright (c) 2014 Øyvind Kolås <pippin@hodefoting.com>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LUZ_STRING_H
#define LUZ_STRING_H

#include "luz-utf8.h"

typedef struct _LuzString LuzString;

struct _LuzString
{
  char *str;
  int   length;
  int   utf8_length;
  int   allocated_length;
}  __attribute((packed));

LuzString   *luz_string_new_with_size  (const char *initial, int initial_size);
LuzString   *luz_string_new            (const char *initial);
void         luz_string_free           (LuzString  *string, int freealloc);
char        *luz_string_dissolve       (LuzString  *string);
const char  *luz_string_get            (LuzString  *string);
int          luz_string_get_length     (LuzString  *string);
int          luz_string_get_utf8_length (LuzString  *string);
void         luz_string_set            (LuzString  *string, const char *new_string);
void         luz_string_clear          (LuzString  *string);
void         luz_string_append_str     (LuzString  *string, const char *str);
void         luz_string_append_byte    (LuzString  *string, char  val);
void         luz_string_append_string  (LuzString  *string, LuzString *string2);
void         luz_string_append_unichar (LuzString  *string, unsigned int unichar);
void         luz_string_append_data    (LuzString  *string, const char *data, int len);
void         luz_string_append_printf  (LuzString  *string, const char *format, ...);
void         luz_string_replace_utf8   (LuzString *string, int pos, const char *new_glyph);
void         luz_string_insert_utf8    (LuzString *string, int pos, const char *new_glyph);
void         luz_string_remove_utf8    (LuzString *string, int pos);


#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#endif
